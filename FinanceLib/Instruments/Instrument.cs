﻿using Database;
using FinanceLib.FinancialCalculators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceLib.Instruments
{
    public enum InstrumentType
    {
        Option,
        Stock
    }

    public static class InstrumentUtils
    {
        public static List<Instrument> LoadAllInstruments()
        {
            var res = new List<Instrument>();
            var db = new InstrumentDB();
            var idList = db.Db_Instrument.Select(r => r.InstrumentId).ToList();
            foreach(var id in idList)
                res.Add(LoadInstrument(id));
            return res;
        }

        public static Instrument LoadInstrument(int instrumentId)
        {
                var db = new InstrumentDB();
                var instrumentType = Enum.Parse(typeof(InstrumentType),
                    db.Db_Instrument.Single(r => r.InstrumentId == instrumentId).InstrumentType);

                switch (instrumentType) {
                case InstrumentType.Option:
                    return OptionUtils.LoadOption(instrumentId);
                case InstrumentType.Stock:
                    return StockUtils.LoadStock(instrumentId);
                default:
                    throw new Exception("Instrument type not implememnted in code: " + instrumentType);
            }
        }

        public static int IsDuplicate(Instrument i)
        {
            var db = new InstrumentDB();
            var displayName = i.GetDisplayName();
            var sameType = db.Db_Instrument.Where(r => r.DisplayType == displayName).ToList();
            if (sameType.Any() && i.GetInstrumentType() == InstrumentType.Stock)
                return sameType.Single(r => r.DisplayType == displayName).InstrumentId;
            //TODO build out option compare class
            //else if (sameType.Any()) {
            //    var idList = sameType.Select(r => r.InstrumentId).ToList();
            //    var sameList = new List<Instrument>();
            //    foreach (var id in idList)
            //        sameList.Add(LoadInstrument(id));
            //}
            return -1;
        }

        public static void RemoveFromDb(int instrumentId)
        {
            using (var db = new InstrumentDB())
            {
                var clearData1 = db.Db_Instrument.SingleOrDefault(r => r.InstrumentId == instrumentId);
                if (clearData1 != null)
                    db.Db_Instrument.Remove(clearData1);

                var clearData2 = db.Inst_Option.SingleOrDefault(r => r.InstrumentId == instrumentId);
                if (clearData2 != null)
                    db.Inst_Option.Remove(clearData2);

                var clearData3 = db.Inst_Option_Digital.SingleOrDefault(r => r.InstrumentId == instrumentId);
                if (clearData3 != null)
                    db.Inst_Option_Digital.Remove(clearData3);

                var clearData4 = db.Inst_Option_Barrier.SingleOrDefault(r => r.InstrumentId == instrumentId);
                if (clearData4 != null)
                    db.Inst_Option_Barrier.Remove(clearData4);

                var clearData5 = db.Inst_Option_Lookback.SingleOrDefault(r => r.InstrumentId == instrumentId);
                if (clearData5 != null)
                    db.Inst_Option_Lookback.Remove(clearData5);

                var clearData6 = db.Inst_Stock.SingleOrDefault(r => r.InstrumentId == instrumentId);
                if (clearData6 != null)
                    db.Inst_Stock.Remove(clearData6);

                db.SaveChanges();
            }
        }
    }

    public abstract class Instrument
    {
        public int InstrumentId;
        public string UnderlyingId;

        public abstract string GetDisplayName();

        public abstract InstrumentType GetInstrumentType();

        public abstract double CalcPrice(DateTime valuationDateTime, double vol);

        public abstract Dictionary<Greeks, double> CalcGreeks(DateTime valuationDateTime, double vol);

        public abstract int SaveToDb();

        protected Db_Instrument GetInstrumentInfo()
        {
            return new Db_Instrument
            {
                InstrumentId = InstrumentId,
                DisplayType = GetDisplayName(),
                InstrumentType = GetInstrumentType().ToString(),
                UnderlyingId = UnderlyingId
            };
        }

        public override string ToString()
        {
            return InstrumentId + " - " + GetDisplayName();
        }
    }
}
