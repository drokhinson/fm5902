﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using FinanceLib.FinancialCalculators;
using FinanceLib.Instruments;

namespace FinanceLib.TradeManager
{
    public enum TradeDirection
    {
        BUY,
        SELL
    }

    public static class TradeManager
    {
        public static List<Trade> LoadAllTrades()
        {
            var res = new List<Trade>();
            using (var db = new PortfolioDB())
            {
                var allTrades = db.Portfolio_Trades.ToList();
                foreach (var t in allTrades)
                    res.Add(new Trade(t));
            }
            return res;
        }

        public static void RemoveFromDb(int tradeId)
        {

            using (var db = new PortfolioDB())
            {
                var clearData1 = db.Portfolio_Trades.SingleOrDefault(r => r.TradeId == tradeId);
                if (clearData1 != null)
                    db.Portfolio_Trades.Remove(clearData1);
                db.SaveChanges();
            }
        }
    }

    public class Trade
    {
        public int TradeId;
        public DateTime TradeDateTime;
        public int InstrumentId { get; set; }
        public double TradePrice;
        public int Quantity;
        public TradeDirection Direction { get; set; }

        public Instrument Instrument;

        public Trade(int instrumentId, DateTime tradeDate, double tradePrice, int quant, bool isBuy)
        {
            TradeDateTime = tradeDate;
            TradePrice = tradePrice;
            Quantity = quant;
            Direction = isBuy ? TradeDirection.BUY : TradeDirection.SELL;

            InstrumentId = instrumentId;
            Instrument = InstrumentUtils.LoadInstrument(instrumentId);
        }

        public Trade(Portfolio_Trades info)
        {
            TradeId = info.TradeId;
            TradeDateTime = info.TradeDateTime;
            InstrumentId = info.InstrumentId;
            TradePrice = info.TradePrice;
            Quantity = info.TradeQuantity;
            Direction = info.IsBuy ? TradeDirection.BUY : TradeDirection.SELL;

            Instrument = InstrumentUtils.LoadInstrument(info.InstrumentId);
        }

        public double CalcPnL(DateTime valuationDateTime, double vol)
        {
            var val = Quantity * (TradePrice - Instrument.CalcPrice(valuationDateTime, vol));
            if (Direction == TradeDirection.SELL)
                return -val;
            return val;
        }

        public double CalcMarketValue(DateTime valuationDateTime, double vol)
        {
            var val = Quantity * Instrument.CalcPrice(valuationDateTime, vol);
            if (Direction == TradeDirection.SELL)
                return -val;
            return  val;
        }

        public Dictionary<Greeks, double> CalcGreekExposure(DateTime valuationDateTime, double vol)
        {
            var res = new Dictionary<Greeks, double>();
            var greeks = Instrument.CalcGreeks(valuationDateTime, vol);
            var direction = Direction == TradeDirection.BUY ? 1.0 : -1.0;
            foreach (var g in greeks.Keys)
                res.Add(g, direction * greeks[g] * Quantity);
            return res;
        }

        public void SaveToDb()
        {
            using (var db = new PortfolioDB())
            {
                var tradeId = db.Portfolio_Trades.Any() ?
                    db.Portfolio_Trades.Max(r => r.TradeId) + 1 : 1;
                var tradeInfo = new Portfolio_Trades
                {
                    TradeId = tradeId,
                    TradeDateTime = TradeDateTime,
                    InstrumentId = InstrumentId,
                    TradePrice = TradePrice,
                    TradeQuantity = Quantity,
                    IsBuy = Direction == TradeDirection.BUY
                };

                db.Portfolio_Trades.Add(tradeInfo);
                db.SaveChanges();
            }
        }
    }
}
