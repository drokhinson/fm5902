﻿using Database;
using FinanceLib.FinancialCalculators;
using FinanceLib.Instruments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceLib.DataManager
{
    public static class MarketDataManager
    {
        public static double GetLatestAvailableSpot(DateTime valuationDateTime, string underlyingId)
        {
            var db = new MarketDB();
            return db.MD_SnapShot.Where(r => r.UnderlyingId == underlyingId &&
                r.ValueDateTime <= valuationDateTime)
                .OrderByDescending(r => r.RunId).First().ClosingPrice;
        }

        public static double GetRate(DateTime valuationDateTime, double tenor)
        {
            using (var db = new MarketDB())
            {
                var runId = db.MD_InterestRate.Where(r => r.EffectiveDate <= valuationDateTime).Max(r => r.RunId);

                //Force to use smallest tenor available in db if option tenor shorter
                var rateData = db.MD_InterestRate.Where(r => r.RunId == runId);
                if (rateData.Min(r => r.Tenor) > tenor)
                    tenor = rateData.Min(r => r.Tenor);

                return db.MD_InterestRate.Where(r => r.RunId == runId && r.Tenor <= tenor)
                    .OrderByDescending(r => r.Tenor).First().Rate;
            }

            //Correct implimenation (pending spline class implementation) 
            using (var db = new MarketDB())
            {
                var runId = db.MD_InterestRate.Where(r => r.EffectiveDate <= valuationDateTime).Max(r => r.RunId);

                var curveData = db.MD_InterestRate.Where(r => r.RunId == runId).ToDictionary(r => r.Tenor, r => r.Rate);
                var data = new double[curveData.Count, curveData.Count];
                int i = 0;
                foreach (var p in curveData.Keys)
                {
                    data[i, 0] = Convert.ToDouble(p);
                    data[i++, 1] = curveData[p];
                }

                return FinanceCalculators.Spline(tenor, data);
            }
        }

        public static List<string> GetAllUnderlying()
        {
            var db = new MarketDB();
            return db.MD_Underlying.Select(r => r.UnderlyingId).ToList();
        }

        public static bool AddUnderlying(string underlyingId, string description, string dataSource)
        {
            using (var db = new MarketDB())
            {
                if (db.MD_Underlying.Where(r => r.UnderlyingId == underlyingId).Any())
                    return false;

                var underlyingInfo = new MD_Underlying
                {
                    UnderlyingId = underlyingId,
                    Description = description,
                    DataSource = dataSource
                };

                db.MD_Underlying.Add(underlyingInfo);
                db.SaveChanges();
            }

            return true;
        }

        public static void RemoveUnderlying(string underlyingId, bool clearPriceData = false)
        {
            using (var db = new MarketDB())
            {
                var clearData1 = db.MD_Underlying.SingleOrDefault(r => r.UnderlyingId == underlyingId);
                if (clearData1 != null)
                    db.MD_Underlying.Remove(clearData1);

                var clearData2 = db.MD_SnapShot.Where(r => r.UnderlyingId == underlyingId);
                if (clearData2 != null)
                    db.MD_SnapShot.RemoveRange(clearData2);
                db.SaveChanges();
            }

            using (var db = new InstrumentDB())
            {
                var instrumentToDelete = db.Db_Instrument.Where(r => r.UnderlyingId == underlyingId)
                    .Select(r => r.InstrumentId).ToList();
                foreach (var id in instrumentToDelete)
                    InstrumentUtils.RemoveFromDb(id);
            }
        }

        public static void AddSpots(DateTime uploadDateTime, Dictionary<DateTime,Dictionary<string, double>> data)
        {
            using (var db = new MarketDB())
            {
                var snapshotInfo = new List<MD_SnapShot>();
                var runId = db.MD_SnapShot.Any() ? db.MD_SnapShot.Max(r => r.RunId) + 1 : 1;
                var undList = db.MD_Underlying.Select(r => r.UnderlyingId).ToList();

                foreach (var date in data.Keys)
                    foreach(var u in data[date].Keys) {
                        if (!undList.Contains(u))
                            AddUnderlying(u, "temp", "n/a");
                        snapshotInfo.Add(new MD_SnapShot
                        {
                            RunId = runId,
                            ValueDateTime = date,
                            UnderlyingId = u,
                            ClosingPrice = data[date][u]
                        });
                    }

                db.MD_SnapShot.AddRange(snapshotInfo);
                db.SaveChanges();
            }
        }

        public static void AddRateCurveData(DateTime addedDate, Dictionary<int, double> data)
        {
            using (var db = new MarketDB())
            {
                var rateInfo = new List<MD_InterestRate>();
                var runId = db.MD_InterestRate.Any() ? db.MD_InterestRate.Max(r => r.RunId) + 1 : 1;
                foreach (var tenor in data.Keys)
                    rateInfo.Add(new MD_InterestRate
                    {
                        RunId = runId,
                        EffectiveDate = addedDate,
                        Tenor = tenor,
                        Rate = data[tenor]
                    });

                db.MD_InterestRate.AddRange(rateInfo);
                db.SaveChanges();
            }
        }
    }
}
