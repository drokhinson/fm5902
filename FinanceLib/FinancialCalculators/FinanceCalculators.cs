﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FinanceLib.FinancialCalculators
{
    public static class FinanceCalculators
    {
        public static double StndErr(List<double> vals, double? estimate = null)
        {
            return Math.Sqrt(Variance(vals, estimate) / vals.Count());
        }

        public static double StndErrAntithetic(List<double> vals, double? estimate = null)
        {
            return Math.Sqrt(3.0/4.0 * Variance(vals, estimate) / (vals.Count()));
        }


        public static double Variance(List<double> vals, double? estimate = null)
        {
            if (estimate == null) 
                estimate = vals.Average();

            return vals.Sum(r => Math.Pow(r - estimate.Value, 2)) / (vals.Count() - 1);
        }

        public static double StndDev(List<double> vals, double? estimate = null)
        {
            double sum = 0.0;
            if (estimate == null) estimate = vals.Average();
            foreach (var n in vals) sum += Math.Pow(n - estimate.Value, 2);

            return Math.Sqrt(sum / (vals.Count() - 1));
        }

        public static double Spline(double xVal, double[,] data)
        {
            throw new NotImplementedException();
        }
    }

    public static class NormalRandom
    {
        //Normally distributed Random number generators
        public static double RndmNumIterative(Random n)
        {
            //Generates normally distributed random number by summing 12 uniformly distributed rnd Nums and subtracting 6
            var normRand = 0.0;
            for (int i = 0; i < 12; i++) normRand += n.NextDouble();
            return normRand - 6;
        }

        public static double[] RndmNumBoxMuller(Random n)
        {
            //Generates list with two normally distributed random numbers using the Box-Muller method
            var res = new double[2];
            var n1 = n.NextDouble();
            var n2 = n.NextDouble();

            res[0] = (Math.Sqrt(-2 * Math.Log(n1)) * Math.Cos(2 * Math.PI * n2));
            res[1] = (Math.Sqrt(-2 * Math.Log(n1)) * Math.Sin(2 * Math.PI * n2));
            return res;
        }

        public static double[] RndmNumPolarRejection(Random n)
        {
            //Generates list with two normally distributed random numbers using the Polar-Rejection method
            var res = new double[2];
            double n1;
            double n2;
            double w;

            do
            {
                n1 = n.NextDouble() * 2 - 1;
                n2 = n.NextDouble() * 2 - 1;
                w = Math.Pow(n1, 2) + Math.Pow(n2, 2);
            } while (w > 1);

            var c = Math.Sqrt(-2 * Math.Log(w) / w);

            res[0] = c * n1;
            res[1] = c * n2;
            return res;
        }

        public static List<double> TransformToJoint(List<double> vals, double corr)
        {
            //Takes list of numbers and returns list with 2 joint normal random numbers
            var res = new List<double>();
            var v1 = vals.First();
            var v2 = vals.Last();

            res.Add(v1);
            res.Add(v1 * corr + Math.Sqrt(1 - Math.Pow(corr, 2)) * v2);
            return vals;
        }
    }
}