﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using PortfolioManagerDR.GUI.AssetEvaluators;
using PortfolioManagerDR.GUI.MarketDataEval;
using PortfolioManagerDR.GUI.Portfolio;

namespace PortfolioManagerDR
{
    public partial class MainForm : Form
    {
        private static readonly Dictionary<string, string[]> Menus = new Dictionary<string, string[]> {
            { "&Portfolio Management", new[] { "&Portfolio" } },
            { "&Asset Evaluation", new[] { "&Option Form" } },
            { "&Database Update", new[] { "&Interest Rate", "&Spot Price", "&Instrument" } },
        };

        private Dictionary<string, Control> ControlsByName = new Dictionary<string, Control> {
            { "Option Form", new OptionEvalForm() }, 
            { "Portfolio", new PortfolioManager() }, 
            { "Interest Rate", new InterestRateEvalForm() }, 
            //{ "Instrument", new InterestRateEvalForm() }, 
            { "Spot Price", new SpotPriceEvalForm() }, 
        };

        public MainForm()
        {
            InitializeComponent();

            int menuLocation = 0;
            var menus = GenerateMenus();
            foreach (var menu in menus)
                menuStrip1.Items.Insert(menuLocation++, menu);
        }

        #region Menu&TabControls
        private List<ToolStripMenuItem> GenerateMenus()
        {
            var menus = new List<ToolStripMenuItem>();

            menus.AddRange(Menus.Select(item => MakeMenu(item.Key, item.Value)));

            return menus;
        }

        private ToolStripMenuItem MakeMenu(string title, IEnumerable<string> menuItems)
        {
            var menu = new ToolStripMenuItem { Text = title };
            foreach (var text in menuItems)
                menu.DropDown.Items.Add(text, null, MenuItem_Click);
            menu.ForeColor = Color.DarkBlue;
            return menu;
        }

        private void MenuItem_Click(object sender, EventArgs e)
        {
            AddTab(sender.ToString().Replace("&", ""));
        }

        private void AddTab(string tabText, bool selectNewTab = true, int position = -1, bool replaceExisting = false)
        {
            if (!ControlsByName.ContainsKey(tabText)) {
                MessageBox.Show("Couldn't open unknown tab \"" + tabText + "\"", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tabControl.TabPages.Cast<TabPage>().Any(x => x.Text == tabText)) {
                var existingTab = tabControl.TabPages.Cast<TabPage>().First(x => x.Text == tabText);
                if (replaceExisting)
                    RemoveTab(existingTab);
                else {
                    if (selectNewTab)
                        tabControl.SelectTab(existingTab);
                    return;
                }
            }

            if (position < 0)
                position = position + 1 + tabControl.TabCount; //count from the right side for negatives. -1 adds at the right end, -2 is second from the right, etc.

            Control newControl = ControlsByName[tabText];
            newControl.Dock = DockStyle.Fill;
            var tabPage = new TabPage { Text = tabText };
            tabPage.Controls.Add(newControl);
            tabControl.TabPages.Insert(position, tabPage);
            tabControl.SelectTab(tabPage);

            tabCloseButton.Enabled = true;
        }

        private void RemoveTab(TabPage tab)
        {
            if (tab == null)
                return;
            /* Select the tab to the left when the active tab is closed, unless it was already the leftmost in which
             * case don't do anything and let it follow the default behavior (select the leftmost tab): */
            if (tab == tabControl.SelectedTab && tabControl.SelectedIndex > 0)
                tabControl.SelectedIndex = tabControl.SelectedIndex - 1;
            tab.Controls.Clear();
            tab.Dispose();

            if (tabControl.TabCount == 0) tabCloseButton.Enabled = false;
        }

        private void tabCloseButton_Click(object sender, EventArgs e)
        {
            RemoveTab(tabControl.SelectedTab);
        }
        #endregion
    }
}
