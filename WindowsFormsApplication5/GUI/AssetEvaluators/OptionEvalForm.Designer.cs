﻿using System.Drawing;
using System.Windows.Forms;

namespace PortfolioManagerDR.GUI.AssetEvaluators
{
    partial class OptionEvalForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.optionTypeComboBox = new System.Windows.Forms.ComboBox();
            this.modelComboBox = new System.Windows.Forms.ComboBox();
            this.spotInput = new System.Windows.Forms.TextBox();
            this.strikeInput = new System.Windows.Forms.TextBox();
            this.rateInput = new System.Windows.Forms.TextBox();
            this.tenorInput = new System.Windows.Forms.TextBox();
            this.divInput = new System.Windows.Forms.TextBox();
            this.volInput = new System.Windows.Forms.TextBox();
            this.CalcButton = new System.Windows.Forms.Button();
            this.Logger = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.putRadioButton = new System.Windows.Forms.RadioButton();
            this.callRadioButton = new System.Windows.Forms.RadioButton();
            this.CalcModeGroupBox = new System.Windows.Forms.GroupBox();
            this.volCalcRadioButton = new System.Windows.Forms.RadioButton();
            this.priceCalcRadioButton = new System.Windows.Forms.RadioButton();
            this.stepsLabel = new System.Windows.Forms.Label();
            this.inputError = new System.Windows.Forms.ErrorProvider(this.components);
            this.simulationGroupBox = new System.Windows.Forms.GroupBox();
            this.calculationLabel = new System.Windows.Forms.Label();
            this.controlVariateCheckBox = new System.Windows.Forms.CheckBox();
            this.antitheticCheckBox = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.scenarioProgressBar = new System.Windows.Forms.ProgressBar();
            this.stepsInput = new System.Windows.Forms.TextBox();
            this.scenariosInput = new System.Windows.Forms.TextBox();
            this.secnarioGenButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.priceInput = new System.Windows.Forms.TextBox();
            this.stepsWarning = new System.Windows.Forms.ErrorProvider(this.components);
            this.multiThreadBox = new System.Windows.Forms.CheckBox();
            this.asianStartLabel = new System.Windows.Forms.Label();
            this.asianStartAvgBox = new System.Windows.Forms.TextBox();
            this.asainLabel1 = new System.Windows.Forms.Label();
            this.asianGroupBox = new System.Windows.Forms.GroupBox();
            this.digitalGroupBox = new System.Windows.Forms.GroupBox();
            this.digitalOptionLabel = new System.Windows.Forms.Label();
            this.digitalRebateBox = new System.Windows.Forms.TextBox();
            this.digitalLabel = new System.Windows.Forms.Label();
            this.asianLabel2 = new System.Windows.Forms.Label();
            this.asianEndAvgBox = new System.Windows.Forms.TextBox();
            this.asianEndLabel = new System.Windows.Forms.Label();
            this.barrierGroupBox = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.barrierValueBox = new System.Windows.Forms.TextBox();
            this.barrierLabel = new System.Windows.Forms.Label();
            this.barrierUpIn = new System.Windows.Forms.RadioButton();
            this.barrierUpOut = new System.Windows.Forms.RadioButton();
            this.barrierDownIn = new System.Windows.Forms.RadioButton();
            this.barrierDownOut = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.CalcModeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputError)).BeginInit();
            this.simulationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepsWarning)).BeginInit();
            this.asianGroupBox.SuspendLayout();
            this.digitalGroupBox.SuspendLayout();
            this.barrierGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // optionTypeComboBox
            // 
            this.optionTypeComboBox.FormattingEnabled = true;
            this.optionTypeComboBox.Location = new System.Drawing.Point(318, 41);
            this.optionTypeComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.optionTypeComboBox.Name = "optionTypeComboBox";
            this.optionTypeComboBox.Size = new System.Drawing.Size(201, 24);
            this.optionTypeComboBox.TabIndex = 5;
            this.optionTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.optionTypeComboBox_SelectedIndexChanged);
            // 
            // modelComboBox
            // 
            this.modelComboBox.FormattingEnabled = true;
            this.modelComboBox.Location = new System.Drawing.Point(566, 41);
            this.modelComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.modelComboBox.Name = "modelComboBox";
            this.modelComboBox.Size = new System.Drawing.Size(195, 24);
            this.modelComboBox.TabIndex = 5;
            this.modelComboBox.SelectedIndexChanged += new System.EventHandler(this.modelComboBox_SelectedIndexChanged);
            // 
            // spotInput
            // 
            this.spotInput.Location = new System.Drawing.Point(141, 101);
            this.spotInput.Margin = new System.Windows.Forms.Padding(4);
            this.spotInput.Name = "spotInput";
            this.spotInput.Size = new System.Drawing.Size(132, 22);
            this.spotInput.TabIndex = 1;
            this.spotInput.TextChanged += new System.EventHandler(this.spotInput_TextChanged);
            // 
            // strikeInput
            // 
            this.strikeInput.Location = new System.Drawing.Point(141, 133);
            this.strikeInput.Margin = new System.Windows.Forms.Padding(4);
            this.strikeInput.Name = "strikeInput";
            this.strikeInput.Size = new System.Drawing.Size(132, 22);
            this.strikeInput.TabIndex = 1;
            this.strikeInput.TextChanged += new System.EventHandler(this.strikeInput_TextChanged);
            // 
            // rateInput
            // 
            this.rateInput.Location = new System.Drawing.Point(141, 165);
            this.rateInput.Margin = new System.Windows.Forms.Padding(4);
            this.rateInput.Name = "rateInput";
            this.rateInput.Size = new System.Drawing.Size(132, 22);
            this.rateInput.TabIndex = 1;
            this.rateInput.TextChanged += new System.EventHandler(this.rateInput_TextChanged);
            // 
            // tenorInput
            // 
            this.tenorInput.Location = new System.Drawing.Point(141, 197);
            this.tenorInput.Margin = new System.Windows.Forms.Padding(4);
            this.tenorInput.Name = "tenorInput";
            this.tenorInput.Size = new System.Drawing.Size(132, 22);
            this.tenorInput.TabIndex = 1;
            this.tenorInput.TextChanged += new System.EventHandler(this.tenorInput_TextChanged);
            // 
            // divInput
            // 
            this.divInput.Location = new System.Drawing.Point(141, 229);
            this.divInput.Margin = new System.Windows.Forms.Padding(4);
            this.divInput.Name = "divInput";
            this.divInput.Size = new System.Drawing.Size(132, 22);
            this.divInput.TabIndex = 1;
            this.divInput.TextChanged += new System.EventHandler(this.divInput_TextChanged);
            // 
            // volInput
            // 
            this.volInput.Location = new System.Drawing.Point(141, 261);
            this.volInput.Margin = new System.Windows.Forms.Padding(4);
            this.volInput.Name = "volInput";
            this.volInput.Size = new System.Drawing.Size(132, 22);
            this.volInput.TabIndex = 1;
            this.volInput.TextChanged += new System.EventHandler(this.volInput_TextChanged);
            // 
            // CalcButton
            // 
            this.CalcButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalcButton.Location = new System.Drawing.Point(70, 293);
            this.CalcButton.Margin = new System.Windows.Forms.Padding(4);
            this.CalcButton.Name = "CalcButton";
            this.CalcButton.Size = new System.Drawing.Size(203, 68);
            this.CalcButton.TabIndex = 0;
            this.CalcButton.Text = "Calculate";
            this.CalcButton.UseVisualStyleBackColor = true;
            this.CalcButton.Click += new System.EventHandler(this.calcButton_Click);
            // 
            // Logger
            // 
            this.Logger.Location = new System.Drawing.Point(318, 72);
            this.Logger.Margin = new System.Windows.Forms.Padding(4);
            this.Logger.Name = "Logger";
            this.Logger.Size = new System.Drawing.Size(556, 335);
            this.Logger.TabIndex = 3;
            this.Logger.Text = "";
            this.Logger.TextChanged += new System.EventHandler(this.Logger_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 104);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Spot";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 136);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Strike";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(92, 168);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Rate";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(86, 200);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Tenor";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 232);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Dividend";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(73, 264);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Volatility";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(316, 21);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Option Type";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(562, 21);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "Model";
            // 
            // putRadioButton
            // 
            this.putRadioButton.AutoSize = true;
            this.putRadioButton.Location = new System.Drawing.Point(220, 73);
            this.putRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.putRadioButton.Name = "putRadioButton";
            this.putRadioButton.Size = new System.Drawing.Size(50, 21);
            this.putRadioButton.TabIndex = 7;
            this.putRadioButton.TabStop = true;
            this.putRadioButton.Text = "Put";
            this.putRadioButton.UseVisualStyleBackColor = true;
            // 
            // callRadioButton
            // 
            this.callRadioButton.AutoSize = true;
            this.callRadioButton.Checked = true;
            this.callRadioButton.Location = new System.Drawing.Point(141, 73);
            this.callRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.callRadioButton.Name = "callRadioButton";
            this.callRadioButton.Size = new System.Drawing.Size(52, 21);
            this.callRadioButton.TabIndex = 7;
            this.callRadioButton.TabStop = true;
            this.callRadioButton.Text = "Call";
            this.callRadioButton.UseVisualStyleBackColor = true;
            // 
            // CalcModeGroupBox
            // 
            this.CalcModeGroupBox.Controls.Add(this.volCalcRadioButton);
            this.CalcModeGroupBox.Controls.Add(this.priceCalcRadioButton);
            this.CalcModeGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalcModeGroupBox.Location = new System.Drawing.Point(882, 72);
            this.CalcModeGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.CalcModeGroupBox.Name = "CalcModeGroupBox";
            this.CalcModeGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.CalcModeGroupBox.Size = new System.Drawing.Size(159, 89);
            this.CalcModeGroupBox.TabIndex = 9;
            this.CalcModeGroupBox.TabStop = false;
            this.CalcModeGroupBox.Text = "Calculation Mode";
            // 
            // volCalcRadioButton
            // 
            this.volCalcRadioButton.AutoSize = true;
            this.volCalcRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.volCalcRadioButton.Location = new System.Drawing.Point(8, 60);
            this.volCalcRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.volCalcRadioButton.Name = "volCalcRadioButton";
            this.volCalcRadioButton.Size = new System.Drawing.Size(112, 21);
            this.volCalcRadioButton.TabIndex = 10;
            this.volCalcRadioButton.TabStop = true;
            this.volCalcRadioButton.Text = "Calc Volatility";
            this.volCalcRadioButton.UseVisualStyleBackColor = true;
            // 
            // priceCalcRadioButton
            // 
            this.priceCalcRadioButton.AutoSize = true;
            this.priceCalcRadioButton.Checked = true;
            this.priceCalcRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceCalcRadioButton.Location = new System.Drawing.Point(8, 31);
            this.priceCalcRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.priceCalcRadioButton.Name = "priceCalcRadioButton";
            this.priceCalcRadioButton.Size = new System.Drawing.Size(92, 21);
            this.priceCalcRadioButton.TabIndex = 9;
            this.priceCalcRadioButton.TabStop = true;
            this.priceCalcRadioButton.Text = "Calc Price";
            this.priceCalcRadioButton.UseVisualStyleBackColor = true;
            this.priceCalcRadioButton.CheckedChanged += new System.EventHandler(this.priceCalcRadioButton_CheckedChanged);
            // 
            // stepsLabel
            // 
            this.stepsLabel.AutoSize = true;
            this.stepsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stepsLabel.Location = new System.Drawing.Point(7, 32);
            this.stepsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.stepsLabel.Name = "stepsLabel";
            this.stepsLabel.Size = new System.Drawing.Size(56, 17);
            this.stepsLabel.TabIndex = 11;
            this.stepsLabel.Text = "# Steps";
            // 
            // inputError
            // 
            this.inputError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.inputError.ContainerControl = this;
            // 
            // simulationGroupBox
            // 
            this.simulationGroupBox.Controls.Add(this.calculationLabel);
            this.simulationGroupBox.Controls.Add(this.controlVariateCheckBox);
            this.simulationGroupBox.Controls.Add(this.antitheticCheckBox);
            this.simulationGroupBox.Controls.Add(this.label9);
            this.simulationGroupBox.Controls.Add(this.scenarioProgressBar);
            this.simulationGroupBox.Controls.Add(this.stepsInput);
            this.simulationGroupBox.Controls.Add(this.scenariosInput);
            this.simulationGroupBox.Controls.Add(this.secnarioGenButton);
            this.simulationGroupBox.Controls.Add(this.label8);
            this.simulationGroupBox.Controls.Add(this.stepsLabel);
            this.simulationGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simulationGroupBox.Location = new System.Drawing.Point(882, 168);
            this.simulationGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simulationGroupBox.Name = "simulationGroupBox";
            this.simulationGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simulationGroupBox.Size = new System.Drawing.Size(249, 239);
            this.simulationGroupBox.TabIndex = 12;
            this.simulationGroupBox.TabStop = false;
            this.simulationGroupBox.Text = "Simulation Info";
            this.simulationGroupBox.Visible = false;
            // 
            // calculationLabel
            // 
            this.calculationLabel.AutoSize = true;
            this.calculationLabel.ForeColor = System.Drawing.Color.Red;
            this.calculationLabel.Location = new System.Drawing.Point(7, 186);
            this.calculationLabel.Name = "calculationLabel";
            this.calculationLabel.Size = new System.Drawing.Size(130, 17);
            this.calculationLabel.TabIndex = 20;
            this.calculationLabel.Text = "Calculating Price";
            this.calculationLabel.Visible = false;
            // 
            // controlVariateCheckBox
            // 
            this.controlVariateCheckBox.AutoSize = true;
            this.controlVariateCheckBox.Checked = true;
            this.controlVariateCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.controlVariateCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.controlVariateCheckBox.Location = new System.Drawing.Point(11, 113);
            this.controlVariateCheckBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.controlVariateCheckBox.Name = "controlVariateCheckBox";
            this.controlVariateCheckBox.Size = new System.Drawing.Size(149, 21);
            this.controlVariateCheckBox.TabIndex = 19;
            this.controlVariateCheckBox.Text = "Use control variate";
            this.controlVariateCheckBox.UseVisualStyleBackColor = true;
            // 
            // antitheticCheckBox
            // 
            this.antitheticCheckBox.AutoSize = true;
            this.antitheticCheckBox.Checked = true;
            this.antitheticCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.antitheticCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.antitheticCheckBox.Location = new System.Drawing.Point(11, 86);
            this.antitheticCheckBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.antitheticCheckBox.Name = "antitheticCheckBox";
            this.antitheticCheckBox.Size = new System.Drawing.Size(179, 21);
            this.antitheticCheckBox.TabIndex = 18;
            this.antitheticCheckBox.Text = "Use antithetic reduction";
            this.antitheticCheckBox.UseVisualStyleBackColor = true;
            this.antitheticCheckBox.CheckedChanged += new System.EventHandler(this.antitheticCheckBox_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(5, 186);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(181, 17);
            this.label9.TabIndex = 17;
            this.label9.Text = "Generating Scenarios...";
            this.label9.Visible = false;
            // 
            // scenarioProgressBar
            // 
            this.scenarioProgressBar.Location = new System.Drawing.Point(5, 206);
            this.scenarioProgressBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.scenarioProgressBar.Name = "scenarioProgressBar";
            this.scenarioProgressBar.Size = new System.Drawing.Size(211, 23);
            this.scenarioProgressBar.TabIndex = 16;
            this.scenarioProgressBar.Visible = false;
            // 
            // stepsInput
            // 
            this.stepsInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stepsInput.Location = new System.Drawing.Point(107, 30);
            this.stepsInput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.stepsInput.Name = "stepsInput";
            this.stepsInput.Size = new System.Drawing.Size(113, 22);
            this.stepsInput.TabIndex = 15;
            this.stepsInput.TextChanged += new System.EventHandler(this.stepsInput_TextChanged);
            // 
            // scenariosInput
            // 
            this.scenariosInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scenariosInput.Location = new System.Drawing.Point(107, 58);
            this.scenariosInput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.scenariosInput.Name = "scenariosInput";
            this.scenariosInput.Size = new System.Drawing.Size(113, 22);
            this.scenariosInput.TabIndex = 15;
            this.scenariosInput.TextChanged += new System.EventHandler(this.scenariosInput_TextChanged);
            // 
            // secnarioGenButton
            // 
            this.secnarioGenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secnarioGenButton.Location = new System.Drawing.Point(107, 140);
            this.secnarioGenButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.secnarioGenButton.Name = "secnarioGenButton";
            this.secnarioGenButton.Size = new System.Drawing.Size(115, 43);
            this.secnarioGenButton.TabIndex = 14;
            this.secnarioGenButton.Text = "Generate New Scenarios";
            this.secnarioGenButton.UseVisualStyleBackColor = true;
            this.secnarioGenButton.Click += new System.EventHandler(this.secnarioGenButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 62);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 17);
            this.label8.TabIndex = 11;
            this.label8.Text = "# Scenarios";
            // 
            // priceInput
            // 
            this.priceInput.Location = new System.Drawing.Point(142, 261);
            this.priceInput.Margin = new System.Windows.Forms.Padding(4);
            this.priceInput.Name = "priceInput";
            this.priceInput.Size = new System.Drawing.Size(132, 22);
            this.priceInput.TabIndex = 13;
            this.priceInput.Visible = false;
            this.priceInput.TextChanged += new System.EventHandler(this.priceInput_TextChanged);
            // 
            // stepsWarning
            // 
            this.stepsWarning.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.stepsWarning.ContainerControl = this;
            // 
            // multiThreadBox
            // 
            this.multiThreadBox.AutoSize = true;
            this.multiThreadBox.Checked = true;
            this.multiThreadBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.multiThreadBox.Location = new System.Drawing.Point(1051, 4);
            this.multiThreadBox.Margin = new System.Windows.Forms.Padding(4);
            this.multiThreadBox.Name = "multiThreadBox";
            this.multiThreadBox.Size = new System.Drawing.Size(172, 21);
            this.multiThreadBox.TabIndex = 14;
            this.multiThreadBox.Text = "Enable MultiThreading";
            this.multiThreadBox.UseVisualStyleBackColor = true;
            this.multiThreadBox.CheckedChanged += new System.EventHandler(this.multiThreadBox_CheckedChanged);
            // 
            // asianStartLabel
            // 
            this.asianStartLabel.AutoSize = true;
            this.asianStartLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asianStartLabel.Location = new System.Drawing.Point(7, 45);
            this.asianStartLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.asianStartLabel.Name = "asianStartLabel";
            this.asianStartLabel.Size = new System.Drawing.Size(106, 17);
            this.asianStartLabel.TabIndex = 11;
            this.asianStartLabel.Text = "Start Averaging";
            // 
            // asianStartAvgBox
            // 
            this.asianStartAvgBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asianStartAvgBox.Location = new System.Drawing.Point(120, 42);
            this.asianStartAvgBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.asianStartAvgBox.Name = "asianStartAvgBox";
            this.asianStartAvgBox.Size = new System.Drawing.Size(113, 22);
            this.asianStartAvgBox.TabIndex = 15;
            // 
            // asainLabel1
            // 
            this.asainLabel1.AutoSize = true;
            this.asainLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asainLabel1.Location = new System.Drawing.Point(7, 17);
            this.asainLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.asainLabel1.Name = "asainLabel1";
            this.asainLabel1.Size = new System.Drawing.Size(262, 17);
            this.asainLabel1.TabIndex = 16;
            this.asainLabel1.Text = "Input which steps span averaging period";
            // 
            // asianGroupBox
            // 
            this.asianGroupBox.Controls.Add(this.asianLabel2);
            this.asianGroupBox.Controls.Add(this.asainLabel1);
            this.asianGroupBox.Controls.Add(this.asianStartAvgBox);
            this.asianGroupBox.Controls.Add(this.asianEndAvgBox);
            this.asianGroupBox.Controls.Add(this.asianEndLabel);
            this.asianGroupBox.Controls.Add(this.asianStartLabel);
            this.asianGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asianGroupBox.Location = new System.Drawing.Point(318, 413);
            this.asianGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.asianGroupBox.Name = "asianGroupBox";
            this.asianGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.asianGroupBox.Size = new System.Drawing.Size(556, 122);
            this.asianGroupBox.TabIndex = 15;
            this.asianGroupBox.TabStop = false;
            this.asianGroupBox.Text = "Exotic Option Info";
            this.asianGroupBox.Visible = false;
            // 
            // digitalGroupBox
            // 
            this.digitalGroupBox.Controls.Add(this.digitalOptionLabel);
            this.digitalGroupBox.Controls.Add(this.digitalRebateBox);
            this.digitalGroupBox.Controls.Add(this.digitalLabel);
            this.digitalGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalGroupBox.Location = new System.Drawing.Point(319, 413);
            this.digitalGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.digitalGroupBox.Name = "digitalGroupBox";
            this.digitalGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.digitalGroupBox.Size = new System.Drawing.Size(556, 122);
            this.digitalGroupBox.TabIndex = 16;
            this.digitalGroupBox.TabStop = false;
            this.digitalGroupBox.Text = "Exotic Option Info";
            this.digitalGroupBox.Visible = false;
            // 
            // digitalOptionLabel
            // 
            this.digitalOptionLabel.AutoSize = true;
            this.digitalOptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalOptionLabel.Location = new System.Drawing.Point(7, 17);
            this.digitalOptionLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.digitalOptionLabel.Name = "digitalOptionLabel";
            this.digitalOptionLabel.Size = new System.Drawing.Size(264, 17);
            this.digitalOptionLabel.TabIndex = 16;
            this.digitalOptionLabel.Text = "Rebate Paid if Option Ends in the Money";
            // 
            // digitalRebateBox
            // 
            this.digitalRebateBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalRebateBox.Location = new System.Drawing.Point(120, 42);
            this.digitalRebateBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.digitalRebateBox.Name = "digitalRebateBox";
            this.digitalRebateBox.Size = new System.Drawing.Size(113, 22);
            this.digitalRebateBox.TabIndex = 15;
            // 
            // digitalLabel
            // 
            this.digitalLabel.AutoSize = true;
            this.digitalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalLabel.Location = new System.Drawing.Point(7, 45);
            this.digitalLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.digitalLabel.Name = "digitalLabel";
            this.digitalLabel.Size = new System.Drawing.Size(54, 17);
            this.digitalLabel.TabIndex = 11;
            this.digitalLabel.Text = "Rebate";
            // 
            // asianLabel2
            // 
            this.asianLabel2.AutoSize = true;
            this.asianLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asianLabel2.Location = new System.Drawing.Point(7, 103);
            this.asianLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.asianLabel2.Name = "asianLabel2";
            this.asianLabel2.Size = new System.Drawing.Size(263, 17);
            this.asianLabel2.TabIndex = 17;
            this.asianLabel2.Text = "If blank default to average over full tenor";
            // 
            // asianEndAvgBox
            // 
            this.asianEndAvgBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asianEndAvgBox.Location = new System.Drawing.Point(120, 72);
            this.asianEndAvgBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.asianEndAvgBox.Name = "asianEndAvgBox";
            this.asianEndAvgBox.Size = new System.Drawing.Size(113, 22);
            this.asianEndAvgBox.TabIndex = 15;
            // 
            // asianEndLabel
            // 
            this.asianEndLabel.AutoSize = true;
            this.asianEndLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asianEndLabel.Location = new System.Drawing.Point(7, 75);
            this.asianEndLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.asianEndLabel.Name = "asianEndLabel";
            this.asianEndLabel.Size = new System.Drawing.Size(101, 17);
            this.asianEndLabel.TabIndex = 11;
            this.asianEndLabel.Text = "End Averaging";
            // 
            // barrierGroupBox
            // 
            this.barrierGroupBox.Controls.Add(this.label13);
            this.barrierGroupBox.Controls.Add(this.label12);
            this.barrierGroupBox.Controls.Add(this.barrierDownOut);
            this.barrierGroupBox.Controls.Add(this.barrierDownIn);
            this.barrierGroupBox.Controls.Add(this.barrierUpOut);
            this.barrierGroupBox.Controls.Add(this.barrierUpIn);
            this.barrierGroupBox.Controls.Add(this.label11);
            this.barrierGroupBox.Controls.Add(this.barrierValueBox);
            this.barrierGroupBox.Controls.Add(this.barrierLabel);
            this.barrierGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrierGroupBox.Location = new System.Drawing.Point(318, 413);
            this.barrierGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.barrierGroupBox.Name = "barrierGroupBox";
            this.barrierGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.barrierGroupBox.Size = new System.Drawing.Size(556, 122);
            this.barrierGroupBox.TabIndex = 17;
            this.barrierGroupBox.TabStop = false;
            this.barrierGroupBox.Text = "Exotic Option Info";
            this.barrierGroupBox.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 28);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(165, 34);
            this.label11.TabIndex = 16;
            this.label11.Text = "Barrier sets spot level at \r\nwhich payoff changes";
            // 
            // barrierValueBox
            // 
            this.barrierValueBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrierValueBox.Location = new System.Drawing.Point(64, 70);
            this.barrierValueBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.barrierValueBox.Name = "barrierValueBox";
            this.barrierValueBox.Size = new System.Drawing.Size(113, 22);
            this.barrierValueBox.TabIndex = 15;
            this.barrierValueBox.TextChanged += new System.EventHandler(this.barrierValueBox_TextChanged);
            // 
            // barrierLabel
            // 
            this.barrierLabel.AutoSize = true;
            this.barrierLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrierLabel.Location = new System.Drawing.Point(6, 73);
            this.barrierLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.barrierLabel.Name = "barrierLabel";
            this.barrierLabel.Size = new System.Drawing.Size(51, 17);
            this.barrierLabel.TabIndex = 11;
            this.barrierLabel.Text = "Barrier";
            // 
            // barrierUpIn
            // 
            this.barrierUpIn.AutoSize = true;
            this.barrierUpIn.Location = new System.Drawing.Point(201, 41);
            this.barrierUpIn.Name = "barrierUpIn";
            this.barrierUpIn.Size = new System.Drawing.Size(99, 21);
            this.barrierUpIn.TabIndex = 17;
            this.barrierUpIn.TabStop = true;
            this.barrierUpIn.Text = "Up and In";
            this.barrierUpIn.UseVisualStyleBackColor = true;
            // 
            // barrierUpOut
            // 
            this.barrierUpOut.AutoSize = true;
            this.barrierUpOut.Location = new System.Drawing.Point(201, 71);
            this.barrierUpOut.Name = "barrierUpOut";
            this.barrierUpOut.Size = new System.Drawing.Size(112, 21);
            this.barrierUpOut.TabIndex = 18;
            this.barrierUpOut.TabStop = true;
            this.barrierUpOut.Text = "Up and Out";
            this.barrierUpOut.UseVisualStyleBackColor = true;
            // 
            // barrierDownIn
            // 
            this.barrierDownIn.AutoSize = true;
            this.barrierDownIn.Location = new System.Drawing.Point(355, 43);
            this.barrierDownIn.Name = "barrierDownIn";
            this.barrierDownIn.Size = new System.Drawing.Size(118, 21);
            this.barrierDownIn.TabIndex = 19;
            this.barrierDownIn.TabStop = true;
            this.barrierDownIn.Text = "Down and In";
            this.barrierDownIn.UseVisualStyleBackColor = true;
            // 
            // barrierDownOut
            // 
            this.barrierDownOut.AutoSize = true;
            this.barrierDownOut.Location = new System.Drawing.Point(355, 69);
            this.barrierDownOut.Name = "barrierDownOut";
            this.barrierDownOut.Size = new System.Drawing.Size(131, 21);
            this.barrierDownOut.TabIndex = 20;
            this.barrierDownOut.TabStop = true;
            this.barrierDownOut.Text = "Down and Out";
            this.barrierDownOut.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(198, 17);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 17);
            this.label12.TabIndex = 21;
            this.label12.Text = "Spot Must Increase";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(352, 17);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(136, 17);
            this.label13.TabIndex = 22;
            this.label13.Text = "Spot Must Decrease";
            // 
            // OptionEvalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.barrierGroupBox);
            this.Controls.Add(this.digitalGroupBox);
            this.Controls.Add(this.asianGroupBox);
            this.Controls.Add(this.multiThreadBox);
            this.Controls.Add(this.priceInput);
            this.Controls.Add(this.simulationGroupBox);
            this.Controls.Add(this.optionTypeComboBox);
            this.Controls.Add(this.modelComboBox);
            this.Controls.Add(this.spotInput);
            this.Controls.Add(this.strikeInput);
            this.Controls.Add(this.rateInput);
            this.Controls.Add(this.tenorInput);
            this.Controls.Add(this.divInput);
            this.Controls.Add(this.volInput);
            this.Controls.Add(this.CalcButton);
            this.Controls.Add(this.CalcModeGroupBox);
            this.Controls.Add(this.callRadioButton);
            this.Controls.Add(this.putRadioButton);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Logger);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "OptionEvalForm";
            this.Size = new System.Drawing.Size(1227, 638);
            this.CalcModeGroupBox.ResumeLayout(false);
            this.CalcModeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputError)).EndInit();
            this.simulationGroupBox.ResumeLayout(false);
            this.simulationGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepsWarning)).EndInit();
            this.asianGroupBox.ResumeLayout(false);
            this.asianGroupBox.PerformLayout();
            this.digitalGroupBox.ResumeLayout(false);
            this.digitalGroupBox.PerformLayout();
            this.barrierGroupBox.ResumeLayout(false);
            this.barrierGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CalcButton;
        private System.Windows.Forms.TextBox spotInput;
        private System.Windows.Forms.TextBox strikeInput;
        private System.Windows.Forms.TextBox rateInput;
        private System.Windows.Forms.TextBox tenorInput;
        private System.Windows.Forms.TextBox divInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox volInput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox Logger;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox optionTypeComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox modelComboBox;
        private System.Windows.Forms.RadioButton putRadioButton;
        private System.Windows.Forms.RadioButton callRadioButton;
        private System.Windows.Forms.GroupBox CalcModeGroupBox;
        private System.Windows.Forms.RadioButton volCalcRadioButton;
        private System.Windows.Forms.RadioButton priceCalcRadioButton;
        private System.Windows.Forms.Label stepsLabel;
        private ErrorProvider inputError;
        private GroupBox simulationGroupBox;
        private Button secnarioGenButton;
        private Label label8;
        private TextBox stepsInput;
        private TextBox scenariosInput;
        private Label label9;
        private ProgressBar scenarioProgressBar;
        private TextBox priceInput;
        private ErrorProvider stepsWarning;
        private CheckBox antitheticCheckBox;
        private CheckBox controlVariateCheckBox;
        private CheckBox multiThreadBox;
        private Label calculationLabel;
        private GroupBox asianGroupBox;
        private Label asainLabel1;
        private TextBox asianStartAvgBox;
        private Label asianStartLabel;
        private GroupBox digitalGroupBox;
        private Label digitalOptionLabel;
        private TextBox digitalRebateBox;
        private Label digitalLabel;
        private Label asianLabel2;
        private TextBox asianEndAvgBox;
        private Label asianEndLabel;
        private GroupBox barrierGroupBox;
        private Label label11;
        private TextBox barrierValueBox;
        private Label barrierLabel;
        private Label label13;
        private Label label12;
        private RadioButton barrierDownOut;
        private RadioButton barrierDownIn;
        private RadioButton barrierUpOut;
        private RadioButton barrierUpIn;
    }
}
