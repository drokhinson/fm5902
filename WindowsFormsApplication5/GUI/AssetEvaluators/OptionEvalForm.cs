﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using FinanceLib.FinancialCalculators;
using FinanceLib.Instruments;
using System.Threading;
using System.Diagnostics;

namespace PortfolioManagerDR.GUI.AssetEvaluators
{
    public partial class OptionEvalForm : UserControl
    {
        public Dictionary<OptionType, List<PricingModel>> AvailableModels = new Dictionary<OptionType, List<PricingModel>>()
        {
            {OptionType.European, new List<PricingModel>{PricingModel.BlackScholes,PricingModel.TrinomialTree, PricingModel.MonteCarlo}},
            {OptionType.American, new List<PricingModel>{PricingModel.TrinomialTree}},
            {OptionType.Asian, new List<PricingModel>{PricingModel.MonteCarlo}},
            {OptionType.Digital, new List<PricingModel>{ PricingModel.BlackScholes, PricingModel.MonteCarlo}},
            {OptionType.Barrier, new List<PricingModel>{PricingModel.MonteCarlo}},
            {OptionType.Lookback, new List<PricingModel>{PricingModel.MonteCarlo}},
            {OptionType.Range, new List<PricingModel>{PricingModel.MonteCarlo}},
        };

        private OptionType OptionType;
        private PricingModel Model;
        private Dictionary<Greeks, double> OptionGreeks;
        private bool IsCalcPrice = true;
        private bool Multithreading = true;

        private int NumSteps;
        private int NumScenarios;
        private List<double[]> Scenarios;
        private List<double[]> RandNums;

        private int ScenarioCounter;
        private readonly int NumCores = Environment.ProcessorCount;
        private readonly Stopwatch CalcTimer = new Stopwatch();
        delegate void progress();

        private double DisplayStdErr;
        private double DisplayPrice;
        private Option DisplayOptionParams;

        public OptionEvalForm()
        {
            
            InitializeComponent();
            PopulateInitialInputs();
            PopulateOptionTypes();
        }

        private void PopulateInitialInputs()
        {
            spotInput.Text = "50";
            strikeInput.Text = "50";
            rateInput.Text = "0.05";
            tenorInput.Text = "1";
            divInput.Text = "0.0";
            volInput.Text = "0.5";
            scenariosInput.Text = "10000";
            stepsInput.Text = "100";
            digitalRebateBox.Text = "5";
            barrierValueBox.Text = "60";
        }


        private void calcButton_Click(object sender, EventArgs e)
        {
            Logger.AppendText("\n------ Starting Calculation ------");
            if(Multithreading)
                Logger.AppendText("\n------ Optimizing Calculations to " + NumCores + " Cores ------");
            Thread t = new Thread(new ThreadStart(Calc));
            t.Start();
        }

        private void Calc()
        {
            CalcTimer.Restart();
            OptionGreeks = new Dictionary<Greeks, double>();
            var option = AssembleInputs();
            var useAntithetic = antitheticCheckBox.Checked;
            var useControlVariate = controlVariateCheckBox.Checked;
            var price = IsCalcPrice ? -1.0 : Convert.ToDouble(priceInput.Text);

            if (CheckInputs(option, price))
            {
                if (Model == PricingModel.BlackScholes)
                {
                    BeginInvoke(new progress(PriceCalc));
                    if (IsCalcPrice) price = BlackScholes.CalcPrice(option);
                    else if (!IsCalcPrice) option.Vol = BlackScholes.CalcVol(option, price);

                    BeginInvoke(new progress(GreeksCalc));
                    if(option.GetOptionType() != OptionType.Digital)
                        OptionGreeks = BlackScholes.AssembleGreeks(option);
                }

                if (Model == PricingModel.TrinomialTree)
                {
                    BeginInvoke(new progress(PriceCalc));
                    if (IsCalcPrice) price = TrinomialTree.CalcPrice(option, NumSteps);
                    else if (!IsCalcPrice) option.Vol = TrinomialTree.CalcVol(option, price);

                    BeginInvoke(new progress(GreeksCalc));
                    OptionGreeks = TrinomialTree.AssembleGreeks(option, NumSteps);
                }

                if (Model == PricingModel.MonteCarlo)
                {
                    BeginInvoke(new progress(PriceCalc));
                    var resList = MonteCarlo.CalcPriceList(option, Scenarios, NumSteps, useControlVariate, Multithreading);
                    DisplayStdErr = useAntithetic ? FinanceCalculators.StndErrAntithetic(resList) : FinanceCalculators.StndErr(resList);
                    price = resList.Average();

                    BeginInvoke(new progress(GreeksCalc));
                    OptionGreeks = MonteCarlo.AssembleGreeks(option, RandNums, Scenarios, NumSteps, NumScenarios, 
                        new VarianceReduction(useAntithetic, useControlVariate), Multithreading, price);
                }

                DisplayPrice = price;
                DisplayOptionParams = option;
                CalcTimer.Stop();

                this.BeginInvoke(new progress(DisplayResults));
            }
        }

        private void secnarioGenButton_Click(object sender, EventArgs e)
        {
            var option = AssembleInputs();
            var price = IsCalcPrice ? -1.0 : Convert.ToDouble(priceInput.Text);
            if (CheckInputs(option, price)) {
                scenarioProgressBar.Maximum = NumScenarios;
                Logger.AppendText("\n------ Generating Scenarios ------");
                label9.Visible = true;
                Thread t = new Thread(() => PrepareScenarios(option));
                t.Start();
            }
        }

        private void PrepareScenarios(Option o)
        {
            //Testing showed that threading will increase execution time for this code because the operation is so fast
            //Only when slowing body of this loop with a for loop did threading increase effieciency of execution.
            var antiRedux = antitheticCheckBox.Checked;

            Scenarios = new List<double[]>();
            RandNums = new List<double[]>();
            var rand = new Random();

            for (ScenarioCounter = 0; ScenarioCounter < NumScenarios; ScenarioCounter++)
            {
                this.BeginInvoke(new progress(UpdateProgress));
                Scenarios.Add(MonteCarlo.GenerateMCPath(o, NumSteps, out var rndNums, rand));
                RandNums.Add(rndNums);
                if (antiRedux)
                    Scenarios.Add(MonteCarlo.GenerateMCPathFromRandNums(o, NumSteps, rndNums, true));
            }
            this.BeginInvoke(new progress(UpdateProgress));
            this.BeginInvoke(new progress(ScenariosGenerated));
        }


        private Option AssembleInputs()
        {
            var spot = Convert.ToDouble(spotInput.Text);
            var stike = Convert.ToDouble(strikeInput.Text);
            var tenor = Convert.ToDouble(tenorInput.Text);
            var rate = Convert.ToDouble(rateInput.Text);
            var div = Convert.ToDouble(divInput.Text);
            var vol = Convert.ToDouble(volInput.Text);
            var isCall = callRadioButton.Checked;
            switch (OptionType)
            {
                case OptionType.European:
                    return new EuropeanOption(spot, stike, tenor, rate, div, vol, isCall);
                case OptionType.American:
                    return new AmericanOption(spot, stike, tenor, rate, div, vol, isCall);
                case OptionType.Asian:
                    Int32.TryParse(asianStartAvgBox.Text, out var startAverage);
                    var endAverage = NumSteps;
                    Int32.TryParse(asianEndAvgBox.Text, out endAverage);
                    return new AsianOption(spot, stike, tenor, rate, div, vol, isCall, startAverage, endAverage);
                case OptionType.Digital:
                    double.TryParse(digitalRebateBox.Text, out var rebate);
                    return new DigitalOption(spot, stike, tenor, rate, div, vol, isCall, rebate);
                case OptionType.Barrier:
                    double.TryParse(barrierValueBox.Text, out var barrier);
                    bool isUp = barrierUpIn.Checked || barrierUpOut.Checked;
                    bool isIn = barrierUpIn.Checked || barrierDownIn.Checked;
                    return new BarrierOption(spot, stike, tenor, rate, div, vol, isCall, barrier, isUp, isIn);
                case OptionType.Lookback:
                    return new LookbackOption(spot, stike, tenor, rate, div, vol, isCall);
                case OptionType.Range:
                    return new RangeOption(spot, stike, tenor, rate, div, vol, isCall);
                default:
                    throw new Exception("Option Type not available");
            }
        }

        private bool CheckInputs(Option option, double price)
        {
            var errorMessage = new List<string>();
            string err = "";

            if (option.Spot < 0.0)
                errorMessage.Add("Spot level can't be < 0");
            if (option.K < 0.0)
                errorMessage.Add("Strike can't be < 0 ");
            if (option.T > 1.0)
                errorMessage.Add("Can not have time to expiration <= 0");
            if (option.R <= 0.0)
                errorMessage.Add("Interest rate can't be < 1");
            if (option.Div < 0.0)
                errorMessage.Add("Dividend can't be < 0");
            if (option.Vol < 0.0 && IsCalcPrice)
                errorMessage.Add("Vol can't be < 0\n");
            if (price < 0.0 && !IsCalcPrice)
                errorMessage.Add("Option Price can't be < 0");
            if (NumSteps < 0.0)
                errorMessage.Add("Number of steps can't be negative");
            if (NumScenarios < 0.0 || NumScenarios > 10000000)
                errorMessage.Add("Number of scenarios must be betwee 0:10000000");

            if (errorMessage.Any())
            {
                foreach (var e in errorMessage) err += (e + "\n");
                inputError.SetError(CalcButton, err);
            } else
                inputError.SetError(CalcButton, "");

            return !errorMessage.Any();
        }

        #region DisplayDelegates

        private void UpdateProgress()
        {
            scenarioProgressBar.Value = ScenarioCounter;
        }

        private void ScenariosGenerated()
        {
            scenarioProgressBar.Value = 0;
            label9.Visible = false;
            CalcButton.Enabled = true;
        }

        private void PriceCalc()
        {
            calculationLabel.Text = "Calculating" + (IsCalcPrice ? " Price..." : "Vol...");
            calculationLabel.Visible = true;
        }

        private void GreeksCalc()
        {
            calculationLabel.Text = "Evaluating Greeks";
        }

        private void DisplayResults()
        {
            calculationLabel.Visible = false;
            var option = DisplayOptionParams;
            var price = DisplayPrice;
            var stdErr = DisplayStdErr;

            var formattedTextOutput = DisplayInputs(option, price);
            formattedTextOutput += DisplayResults(price, stdErr, option.Vol);
            Logger.AppendText("\n\n" + formattedTextOutput);
            Logger.AppendText("Calculation time: " + CalcTimer.ElapsedMilliseconds / 1000 + " sec\n");
        }

        private string DisplayInputs(Option option, double price)
        {
            var s = option.Spot;
            var k = option.K;
            var t = option.T;
            var r = option.R;
            var div = option.Div;
            var vol = option.Vol;
            var isCall = option.IsCall ? "Call" : "Put";
            var optionType = option.GetOptionType();

            var formattedTextOutput = $"------ {optionType} {isCall} Option Inputs ------\n" +
                                         $"\tSpot = ${s}\n" +
                                         $"\tStrike = ${k}\n" +
                                         $"\tTenor = {t} year(s)\n" +
                                         $"\tRate = {r}\n" +
                                         $"\tDividend = {div}\n";
            formattedTextOutput += IsCalcPrice ? $"\tVolatility = {vol}\n" : $"\tPrice = {price}\n";

            return formattedTextOutput;
        }

        private string DisplayResults(double price, double stdErr, double vol)
        {
            var formattedTextOutput = $"\n------ {Model.ToString()} Calculated Results ------\n";
            formattedTextOutput += IsCalcPrice ? $"\tPrice = {price}\n" : $"\tVolatility = {vol}\n\n";
            if(OptionGreeks == null)
                return formattedTextOutput;

            foreach (var g in OptionGreeks.Keys)
                formattedTextOutput += $"\t{g.ToString()} = {OptionGreeks[g]}\n";
            if (Model == PricingModel.MonteCarlo)
                formattedTextOutput += $"\nScenario standard error = {stdErr}\n";

            return formattedTextOutput;
        }

        #endregion

        private void PopulateOptionTypes()
        {
            optionTypeComboBox.Items.AddRange(Enum.GetNames(typeof(OptionType)));
            optionTypeComboBox.SelectedIndex = 0;
        }

        private void priceCalcRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            IsCalcPrice = priceCalcRadioButton.Checked;
            priceInput.Visible = !IsCalcPrice;
            volInput.Visible = IsCalcPrice;
            label6.Text = priceCalcRadioButton.Checked ? "Volatility" : "Option Price";
            label6.Location = new Point(volInput.Location.X - 7 - label6.Size.Width, label5.Location.Y + 32);
        }

        private void optionTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Enum.TryParse(optionTypeComboBox.Text, out OptionType);
            UpdateAvailablePricingModels();
            asianGroupBox.Visible = OptionType == OptionType.Asian;
            digitalGroupBox.Visible = OptionType == OptionType.Digital;
            barrierGroupBox.Visible = OptionType == OptionType.Barrier;
            if (OptionType == OptionType.Barrier)
                BarrierGroupBoxLogic();
            putRadioButton.Enabled = OptionType != OptionType.Range;
            callRadioButton.Enabled = OptionType != OptionType.Range;
        }

        private void UpdateAvailablePricingModels()
        {
            modelComboBox.Items.Clear();
            var models = AvailableModels[OptionType];
            foreach(var m in models)
                modelComboBox.Items.Add(m.ToString());
            modelComboBox.SelectedIndex = 0;
        }

        private void modelComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Enum.TryParse(modelComboBox.Text, out Model);

            if (Model == PricingModel.MonteCarlo)  {
                priceCalcRadioButton.Checked = true;
                volCalcRadioButton.Enabled = false;
            }
            else
                volCalcRadioButton.Enabled = true;

            switch (Model)
            {
                case PricingModel.TrinomialTree:
                case PricingModel.MonteCarlo:
                    simulationGroupBox.Visible = true;
                    secnarioGenButton.Visible = Model == PricingModel.MonteCarlo;
                    scenarioProgressBar.Visible = Model == PricingModel.MonteCarlo;
                    scenariosInput.Visible = Model == PricingModel.MonteCarlo;
                    label8.Visible = Model == PricingModel.MonteCarlo;
                    CalcButton.Enabled = Model != PricingModel.MonteCarlo;
                    stepsInput.Text = 
                        OptionType == OptionType.European || OptionType == OptionType.Digital ? "1" : "252";
                    scenariosInput.Text = OptionType == OptionType.European ? "1000000" : "10000";
                    if (OptionType != OptionType.European)
                        stepsWarning.SetError(stepsInput, null);
                    return;
                default:
                    simulationGroupBox.Visible = false;
                    return;
            }
        }

        #region Parse Inputs

        private void spotInput_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(spotInput.Text, out var s);
            spotInput.BackColor = parse ? Color.White : Color.Red;
            BarrierGroupBoxLogic();
        }

        private void strikeInput_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(strikeInput.Text, out var k);
            strikeInput.BackColor = parse ? Color.White : Color.Red;
        }

        private void tenorInput_TextChanged(object sender, EventArgs e)
        {
            double t;
            var parse = double.TryParse(tenorInput.Text, out t);
            tenorInput.BackColor = parse ? Color.White : Color.Red;
            CalcButton.Enabled = Model != PricingModel.MonteCarlo && parse;
        }

        private void rateInput_TextChanged(object sender, EventArgs e)
        {
            double r;
            var parse = double.TryParse(rateInput.Text, out r);
            rateInput.BackColor = parse ? Color.White : Color.Red;
            CalcButton.Enabled = Model != PricingModel.MonteCarlo && parse;
        }

        private void divInput_TextChanged(object sender, EventArgs e)
        {
            double div;
            var parse = double.TryParse(divInput.Text, out div);
            divInput.BackColor = parse ? Color.White : Color.Red;
            CalcButton.Enabled = Model != PricingModel.MonteCarlo && parse;
        }

        private void volInput_TextChanged(object sender, EventArgs e)
        {
            double vol;
            var parse = double.TryParse(volInput.Text, out vol);
            volInput.BackColor = parse ? Color.White : Color.Green;
            CalcButton.Enabled = Model != PricingModel.MonteCarlo && parse;
        }

        private void priceInput_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(priceInput.Text, out var price);
            priceInput.BackColor = parse ? Color.White : Color.Red;
        }

        private void stepsInput_TextChanged(object sender, EventArgs e)
        {
            var parse = int.TryParse(stepsInput.Text, out NumSteps);
            stepsInput.BackColor = parse ? Color.White : Color.Red;
            if (OptionType == OptionType.European && NumSteps != 1)
                stepsWarning.SetError(stepsInput, "Reduced efficiency if # steps != 1");
            else
                stepsWarning.SetError(stepsInput, null);
            CalcButton.Enabled = Model != PricingModel.MonteCarlo && parse;
            asianEndAvgBox.Text = NumSteps.ToString();
        }

        private void scenariosInput_TextChanged(object sender, EventArgs e)
        {
            var parse = int.TryParse(scenariosInput.Text, out NumScenarios);
            scenariosInput.BackColor = parse ? Color.White : Color.Red;
            CalcButton.Enabled = Model != PricingModel.MonteCarlo && parse;
        }

        private void barrierValueBox_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(spotInput.Text, out var b);
            barrierValueBox.BackColor = parse ? Color.White : Color.Red;
            BarrierGroupBoxLogic();
        }
        #endregion

        private void Logger_TextChanged(object sender, EventArgs e)
        {
            Logger.SelectionStart = Logger.Text.Length;
            Logger.ScrollToCaret();
        }

        private void antitheticCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CalcButton.Enabled = false;
        }

        private void multiThreadBox_CheckedChanged(object sender, EventArgs e)
        {
            Multithreading = multiThreadBox.Checked;
        }

        private void BarrierGroupBoxLogic()
        {
            if(double.TryParse(spotInput.Text, out double spot) && 
                double.TryParse(barrierValueBox.Text, out double barrier))
            {
                barrierDownIn.Enabled = spot > barrier;
                barrierDownOut.Enabled = spot > barrier;
                barrierUpIn.Checked = spot < barrier;

                barrierUpIn.Enabled = spot < barrier;
                barrierUpOut.Enabled = spot < barrier;
                barrierDownIn.Checked = spot > barrier;
            }

        } 
    }
}
