﻿namespace PortfolioManagerDR.GUI.MarketDataEval
{
    partial class InterestRateEvalForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rateGridView = new System.Windows.Forms.DataGridView();
            this.RunIdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EffectiveDateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenorCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.runIdBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.effectiveDatePicker = new System.Windows.Forms.DateTimePicker();
            this.tenorBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rateBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.deleteRowsButton = new System.Windows.Forms.Button();
            this.writeToDbButton = new System.Windows.Forms.Button();
            this.addRowButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.rateGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // rateGridView
            // 
            this.rateGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rateGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RunIdCol,
            this.EffectiveDateCol,
            this.TenorCol,
            this.RateCol});
            this.rateGridView.Location = new System.Drawing.Point(205, 356);
            this.rateGridView.Name = "rateGridView";
            this.rateGridView.ReadOnly = true;
            this.rateGridView.RowTemplate.Height = 40;
            this.rateGridView.Size = new System.Drawing.Size(954, 631);
            this.rateGridView.TabIndex = 0;
            // 
            // RunIdCol
            // 
            this.RunIdCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RunIdCol.HeaderText = "RunId";
            this.RunIdCol.Name = "RunIdCol";
            this.RunIdCol.ReadOnly = true;
            // 
            // EffectiveDateCol
            // 
            this.EffectiveDateCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EffectiveDateCol.HeaderText = "Effective Date";
            this.EffectiveDateCol.Name = "EffectiveDateCol";
            this.EffectiveDateCol.ReadOnly = true;
            // 
            // TenorCol
            // 
            this.TenorCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TenorCol.HeaderText = "Tenor";
            this.TenorCol.Name = "TenorCol";
            this.TenorCol.ReadOnly = true;
            // 
            // RateCol
            // 
            this.RateCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RateCol.HeaderText = "Rate";
            this.RateCol.Name = "RateCol";
            this.RateCol.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(301, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "RunId";
            // 
            // runIdBox
            // 
            this.runIdBox.Enabled = false;
            this.runIdBox.Location = new System.Drawing.Point(428, 51);
            this.runIdBox.Name = "runIdBox";
            this.runIdBox.Size = new System.Drawing.Size(189, 38);
            this.runIdBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(199, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "Effective Date";
            // 
            // effectiveDatePicker
            // 
            this.effectiveDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.effectiveDatePicker.Location = new System.Drawing.Point(428, 127);
            this.effectiveDatePicker.Name = "effectiveDatePicker";
            this.effectiveDatePicker.Size = new System.Drawing.Size(189, 38);
            this.effectiveDatePicker.TabIndex = 4;
            this.effectiveDatePicker.ValueChanged += new System.EventHandler(this.effectiveDatePicker_ValueChanged);
            // 
            // tenorBox
            // 
            this.tenorBox.Location = new System.Drawing.Point(428, 201);
            this.tenorBox.Name = "tenorBox";
            this.tenorBox.Size = new System.Drawing.Size(189, 38);
            this.tenorBox.TabIndex = 6;
            this.tenorBox.TextChanged += new System.EventHandler(this.tenorBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(303, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 32);
            this.label3.TabIndex = 5;
            this.label3.Text = "Tenor";
            // 
            // rateBox
            // 
            this.rateBox.Location = new System.Drawing.Point(428, 275);
            this.rateBox.Name = "rateBox";
            this.rateBox.Size = new System.Drawing.Size(189, 38);
            this.rateBox.TabIndex = 8;
            this.rateBox.TextChanged += new System.EventHandler(this.rateBox_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(316, 278);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 32);
            this.label4.TabIndex = 7;
            this.label4.Text = "Rate";
            // 
            // deleteRowsButton
            // 
            this.deleteRowsButton.Location = new System.Drawing.Point(205, 1022);
            this.deleteRowsButton.Name = "deleteRowsButton";
            this.deleteRowsButton.Size = new System.Drawing.Size(315, 93);
            this.deleteRowsButton.TabIndex = 9;
            this.deleteRowsButton.Text = "Remove Selected Rows";
            this.deleteRowsButton.UseVisualStyleBackColor = true;
            this.deleteRowsButton.Click += new System.EventHandler(this.deleteRowsButton_Click);
            // 
            // writeToDbButton
            // 
            this.writeToDbButton.Location = new System.Drawing.Point(844, 1022);
            this.writeToDbButton.Name = "writeToDbButton";
            this.writeToDbButton.Size = new System.Drawing.Size(315, 93);
            this.writeToDbButton.TabIndex = 10;
            this.writeToDbButton.Text = "Update Rows to Db";
            this.writeToDbButton.UseVisualStyleBackColor = true;
            this.writeToDbButton.Click += new System.EventHandler(this.writeToDbButton_Click);
            // 
            // addRowButton
            // 
            this.addRowButton.Enabled = false;
            this.addRowButton.Location = new System.Drawing.Point(844, 127);
            this.addRowButton.Name = "addRowButton";
            this.addRowButton.Size = new System.Drawing.Size(315, 112);
            this.addRowButton.TabIndex = 11;
            this.addRowButton.Text = "Add Row";
            this.addRowButton.UseVisualStyleBackColor = true;
            this.addRowButton.Click += new System.EventHandler(this.addRowButton_Click);
            // 
            // InterestRateEvalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.addRowButton);
            this.Controls.Add(this.writeToDbButton);
            this.Controls.Add(this.deleteRowsButton);
            this.Controls.Add(this.rateBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tenorBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.effectiveDatePicker);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.runIdBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rateGridView);
            this.Name = "InterestRateEvalForm";
            this.Size = new System.Drawing.Size(2453, 1235);
            ((System.ComponentModel.ISupportInitialize)(this.rateGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView rateGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox runIdBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker effectiveDatePicker;
        private System.Windows.Forms.TextBox tenorBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox rateBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button deleteRowsButton;
        private System.Windows.Forms.Button writeToDbButton;
        private System.Windows.Forms.Button addRowButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn RunIdCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn EffectiveDateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenorCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn RateCol;
    }
}
