﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database;

namespace PortfolioManagerDR.GUI.MarketDataEval
{
    public partial class InterestRateEvalForm : UserControl
    {
        public int RunId;
        public DateTime EffectiveDate;
        public int Tenor;
        public double Rate;

        bool InputT = false;
        bool InputR = false;

        public InterestRateEvalForm()
        {
            InitializeComponent();
            GetRunId();
            EffectiveDate = effectiveDatePicker.Value;
        }

        private void GetRunId()
        {
            var db = new MarketDB();
            if (!db.MD_InterestRate.Any())
                RunId = 1;
            else
                RunId = db.MD_InterestRate.Max(r => r.RunId) + 1;

            runIdBox.Text = RunId.ToString();
        }

        private void effectiveDatePicker_ValueChanged(object sender, EventArgs e)
        {
            EffectiveDate = effectiveDatePicker.Value;
            for (int i = 0; i < rateGridView.Rows.Count; i++)
                rateGridView.Rows[i].Cells[1].Value = EffectiveDate;
        }

        private void tenorBox_TextChanged(object sender, EventArgs e)
        {
            var parse = int.TryParse(tenorBox.Text, out  Tenor);
            tenorBox.BackColor = parse ? Color.White : Color.Red;
            InputT = parse;
            CheckInputs();
        }

        private void rateBox_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(rateBox.Text, out Rate);
            rateBox.BackColor = parse ? Color.White : Color.Red;
            InputR = parse;
            CheckInputs();
        }

        private void deleteRowsButton_Click(object sender, EventArgs e)
        {
            var selectedRows = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in rateGridView.SelectedRows)
                selectedRows.Add(row);

            foreach (DataGridViewRow row in selectedRows)
                rateGridView.Rows.Remove(row);
        }

        private void writeToDbButton_Click(object sender, EventArgs e)
        {
            //TODO Add error trapping for duplicates
            var saveList = new List<MD_InterestRate>();
            for (int i = 0; i < rateGridView.Rows.Count - 1; i++)
                saveList.Add(new MD_InterestRate
                {
                    RunId = (int)rateGridView.Rows[i].Cells[0].Value,
                    EffectiveDate = (DateTime)rateGridView.Rows[i].Cells[1].Value,
                    Tenor = (int)rateGridView.Rows[i].Cells[2].Value,
                    Rate = (double)rateGridView.Rows[i].Cells[3].Value,
                });


            var db = new MarketDB();
            db.MD_InterestRate.AddRange(saveList);
            db.SaveChanges();

            string message = "Data saved to DB. RunId: " + RunId;
            MessageBox.Show(message, "Saved", MessageBoxButtons.OK);
            rateGridView.Rows.Clear();
            GetRunId();
        }

        private void CheckInputs()
        {
            addRowButton.Enabled = InputT && InputR;
        }

        private void addRowButton_Click(object sender, EventArgs e)
        {
            rateGridView.Rows.Add(RunId, EffectiveDate, Tenor, Rate);
        }
    }
}
