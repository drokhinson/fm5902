﻿namespace PortfolioManagerDR.GUI.MarketDataEval
{
    partial class SpotPriceEvalForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addRowButton = new System.Windows.Forms.Button();
            this.writeToDbButton = new System.Windows.Forms.Button();
            this.deleteRowsButton = new System.Windows.Forms.Button();
            this.closePriceBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.underlyingBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.effectiveDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.runIdBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rateGridView = new System.Windows.Forms.DataGridView();
            this.RunIdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValueDateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenorCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.rateGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // addRowButton
            // 
            this.addRowButton.Enabled = false;
            this.addRowButton.Location = new System.Drawing.Point(810, 124);
            this.addRowButton.Name = "addRowButton";
            this.addRowButton.Size = new System.Drawing.Size(315, 112);
            this.addRowButton.TabIndex = 23;
            this.addRowButton.Text = "Add Row";
            this.addRowButton.UseVisualStyleBackColor = true;
            this.addRowButton.Click += new System.EventHandler(this.addRowButton_Click);
            // 
            // writeToDbButton
            // 
            this.writeToDbButton.Location = new System.Drawing.Point(810, 1019);
            this.writeToDbButton.Name = "writeToDbButton";
            this.writeToDbButton.Size = new System.Drawing.Size(315, 93);
            this.writeToDbButton.TabIndex = 22;
            this.writeToDbButton.Text = "Update Rows to Db";
            this.writeToDbButton.UseVisualStyleBackColor = true;
            this.writeToDbButton.Click += new System.EventHandler(this.writeToDbButton_Click);
            // 
            // deleteRowsButton
            // 
            this.deleteRowsButton.Location = new System.Drawing.Point(171, 1019);
            this.deleteRowsButton.Name = "deleteRowsButton";
            this.deleteRowsButton.Size = new System.Drawing.Size(315, 93);
            this.deleteRowsButton.TabIndex = 21;
            this.deleteRowsButton.Text = "Remove Selected Rows";
            this.deleteRowsButton.UseVisualStyleBackColor = true;
            this.deleteRowsButton.Click += new System.EventHandler(this.deleteRowsButton_Click);
            // 
            // closePriceBox
            // 
            this.closePriceBox.Location = new System.Drawing.Point(394, 272);
            this.closePriceBox.Name = "closePriceBox";
            this.closePriceBox.Size = new System.Drawing.Size(189, 38);
            this.closePriceBox.TabIndex = 20;
            this.closePriceBox.TextChanged += new System.EventHandler(this.closePriceBox_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(174, 275);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(183, 32);
            this.label4.TabIndex = 19;
            this.label4.Text = "Closing Price";
            // 
            // underlyingBox
            // 
            this.underlyingBox.Location = new System.Drawing.Point(394, 198);
            this.underlyingBox.Name = "underlyingBox";
            this.underlyingBox.Size = new System.Drawing.Size(189, 38);
            this.underlyingBox.TabIndex = 18;
            this.underlyingBox.TextChanged += new System.EventHandler(this.underlyingBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(175, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(182, 32);
            this.label3.TabIndex = 17;
            this.label3.Text = "Underlying Id";
            // 
            // effectiveDatePicker
            // 
            this.effectiveDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.effectiveDatePicker.Location = new System.Drawing.Point(394, 124);
            this.effectiveDatePicker.Name = "effectiveDatePicker";
            this.effectiveDatePicker.Size = new System.Drawing.Size(189, 38);
            this.effectiveDatePicker.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(208, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 32);
            this.label2.TabIndex = 15;
            this.label2.Text = "ValueDate";
            // 
            // runIdBox
            // 
            this.runIdBox.Enabled = false;
            this.runIdBox.Location = new System.Drawing.Point(394, 48);
            this.runIdBox.Name = "runIdBox";
            this.runIdBox.Size = new System.Drawing.Size(189, 38);
            this.runIdBox.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(267, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 32);
            this.label1.TabIndex = 13;
            this.label1.Text = "RunId";
            // 
            // rateGridView
            // 
            this.rateGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rateGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RunIdCol,
            this.ValueDateCol,
            this.TenorCol,
            this.RateCol});
            this.rateGridView.Location = new System.Drawing.Point(171, 353);
            this.rateGridView.Name = "rateGridView";
            this.rateGridView.ReadOnly = true;
            this.rateGridView.RowTemplate.Height = 40;
            this.rateGridView.Size = new System.Drawing.Size(954, 631);
            this.rateGridView.TabIndex = 12;
            // 
            // RunIdCol
            // 
            this.RunIdCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RunIdCol.HeaderText = "RunId";
            this.RunIdCol.Name = "RunIdCol";
            this.RunIdCol.ReadOnly = true;
            // 
            // ValueDateCol
            // 
            this.ValueDateCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ValueDateCol.HeaderText = "Value Date";
            this.ValueDateCol.Name = "ValueDateCol";
            this.ValueDateCol.ReadOnly = true;
            // 
            // TenorCol
            // 
            this.TenorCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TenorCol.HeaderText = "UnderlyingId";
            this.TenorCol.Name = "TenorCol";
            this.TenorCol.ReadOnly = true;
            // 
            // RateCol
            // 
            this.RateCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RateCol.HeaderText = "Closing Price";
            this.RateCol.Name = "RateCol";
            this.RateCol.ReadOnly = true;
            // 
            // SpotPriceEvalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.addRowButton);
            this.Controls.Add(this.writeToDbButton);
            this.Controls.Add(this.deleteRowsButton);
            this.Controls.Add(this.closePriceBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.underlyingBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.effectiveDatePicker);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.runIdBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rateGridView);
            this.Name = "SpotPriceEvalForm";
            this.Size = new System.Drawing.Size(2453, 1235);
            ((System.ComponentModel.ISupportInitialize)(this.rateGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addRowButton;
        private System.Windows.Forms.Button writeToDbButton;
        private System.Windows.Forms.Button deleteRowsButton;
        private System.Windows.Forms.TextBox closePriceBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox underlyingBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker effectiveDatePicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox runIdBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView rateGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn RunIdCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValueDateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenorCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn RateCol;
    }
}
