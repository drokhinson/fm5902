﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Database;
using FinanceLib.DataManager;

namespace PortfolioManagerDR.GUI.MarketDataEval
{
    public partial class SpotPriceEvalForm : UserControl
    {
        public int RunId;
        public DateTime ValueDate;
        public string UnderlyingId;
        public double ClosingPrice;

        bool InputU = false;
        bool InputC = false;

        public SpotPriceEvalForm()
        {
            InitializeComponent();
            GetRunId();
            ValueDate = effectiveDatePicker.Value;
        }

        private void GetRunId()
        {
            var db = new MarketDB();
            if (!db.MD_SnapShot.Any())
                RunId = 1;
            else
                RunId = db.MD_SnapShot.Max(r => r.RunId) + 1;

            runIdBox.Text = RunId.ToString();
        }

        private void CheckInputs()
        {
            addRowButton.Enabled = InputU && InputC;
        }

        private void underlyingBox_TextChanged(object sender, EventArgs e)
        {
            UnderlyingId = underlyingBox.Text;
            var parse = UnderlyingId.TrimEnd(' ') != "";
            underlyingBox.BackColor = parse ? Color.White : Color.Red;
            InputU = parse;
            CheckInputs();
        }

        private void closePriceBox_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(closePriceBox.Text, out ClosingPrice);
            closePriceBox.BackColor = parse ? Color.White : Color.Red;
            InputC = parse;
            CheckInputs();
        }

        private void addRowButton_Click(object sender, EventArgs e)
        {
            rateGridView.Rows.Add(RunId, ValueDate, UnderlyingId, ClosingPrice);
        }

        private void deleteRowsButton_Click(object sender, EventArgs e)
        {
            var selectedRows = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in rateGridView.SelectedRows)
                selectedRows.Add(row);

            foreach (DataGridViewRow row in selectedRows)
                rateGridView.Rows.Remove(row);
        }

        private void writeToDbButton_Click(object sender, EventArgs e)
        {
            //TODO Add error trapping for duplicates
            var saveList = new List<MD_SnapShot>();
            for (int i = 0; i < rateGridView.Rows.Count - 1; i++)
                saveList.Add(new MD_SnapShot
                {
                    RunId = (int)rateGridView.Rows[i].Cells[0].Value,
                    ValueDateTime = (DateTime)rateGridView.Rows[i].Cells[1].Value,
                    UnderlyingId = (string)rateGridView.Rows[i].Cells[2].Value,
                    ClosingPrice = (double)rateGridView.Rows[i].Cells[3].Value,
                });

            var db = new MarketDB();
            var undList = db.MD_Underlying.Select(r => r.UnderlyingId).ToList();

            foreach (var u in saveList.Select(r => r.UnderlyingId).Distinct())
                if (!undList.Contains(u))
                    MarketDataManager.AddUnderlying(u, "temp", "n/a");
            db.MD_SnapShot.AddRange(saveList);
            db.SaveChanges();

            string message = "Data saved to DB. RunId: " + RunId;
            MessageBox.Show(message, "Saved", MessageBoxButtons.OK);
            rateGridView.Rows.Clear();
            GetRunId();
        }
    }
}
