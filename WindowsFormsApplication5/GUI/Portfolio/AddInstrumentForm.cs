﻿using Database;
using FinanceLib.DataManager;
using FinanceLib.Instruments;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortfolioManagerDR.GUI.Portfolio
{
    public partial class AddInstrumentForm : Form
    {
        private string UnderlyingId;

        private OptionType OType;
        private double Strike;
        private bool IsUp;
        private bool IsIn;
        private double Rebate;
        private double Barrier;

        private string StockBBG;
        private string StockCompany;
        private string StockDescription;

        private Instrument Instrument;

        public AddInstrumentForm()
        {
            InitializeComponent();
            PopulateComboBoxes();
        }

        private void PopulateComboBoxes()
        {
            foreach (var i in Enum.GetValues(typeof(InstrumentType)))
                instrumentTypeComboBox.Items.Add(i);

            foreach (var i in Enum.GetValues(typeof(OptionType)))
                optionTypeCombo.Items.Add(i);

            foreach (var i in MarketDataManager.GetAllUnderlying())
                underlyingComboBox.Items.Add(i);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            var instrumentInfo = new Db_Instrument
            {
                UnderlyingId = UnderlyingId,
                InstrumentId = -1
            };

            var iType = (InstrumentType)instrumentTypeComboBox.SelectedItem;
            if(iType == InstrumentType.Stock)
                    Instrument = new Stock(UnderlyingId, StockCompany, StockBBG, StockDescription);
            if (iType == InstrumentType.Option)
            {
                var optionInfo = new Inst_Option()
                {
                    Strike = Strike,
                    ExpirationDate = optionExpirePicker.Value.Date,
                    IsCall = optionIsCall.Checked,
                    OptionType = OType.ToString()
                };

                switch (OType)
                {
                    case OptionType.European:
                        Instrument = new EuropeanOption(instrumentInfo, optionInfo);
                        break;
                    case OptionType.American:
                        Instrument = new AmericanOption(instrumentInfo, optionInfo);
                        break;
                    case OptionType.Asian:
                        Instrument = new AsianOption(instrumentInfo, optionInfo);
                        break;
                    case OptionType.Lookback:
                        Instrument = new LookbackOption(instrumentInfo, optionInfo, false);
                        break;
                    case OptionType.Range:
                        Instrument = new RangeOption(instrumentInfo, optionInfo);
                        break;
                    case OptionType.Digital:
                        Instrument = new DigitalOption(instrumentInfo, optionInfo, Rebate);
                        break;
                    case OptionType.Barrier:
                        var barrierInfo = new Inst_Option_Barrier
                        {
                            Barrier = Barrier,
                            IsIn = IsIn,
                            IsUp = IsUp
                        };
                        Instrument = new BarrierOption(instrumentInfo, optionInfo, barrierInfo);
                        break;
                }
            }

            Instrument.SaveToDb();
        }

        private void instrumentTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            stockInfoBox.Visible = false;
            optionInfoBox.Visible = false;
            digitalGroupBox.Visible = false;
            barrierGroupBox.Visible = false;

            switch (instrumentTypeComboBox.SelectedItem)
            {
                case (InstrumentType.Stock):
                    stockInfoBox.Visible = true;
                    break;
                case (InstrumentType.Option):
                    optionInfoBox.Visible = true;
                    break;
            }
        }

        private void optionTypeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            OType = (OptionType)optionTypeCombo.SelectedItem;
            barrierGroupBox.Visible = OType == OptionType.Barrier;
            digitalGroupBox.Visible = OType == OptionType.Digital;
        }

        private void optionStrikeBox_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(optionStrikeBox.Text, out Strike);
            optionStrikeBox.BackColor = parse ? Color.White : Color.Red;
        }

        private void underlyingComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UnderlyingId = underlyingComboBox.SelectedItem.ToString();
            underlyingValBox.Text = MarketDataManager.GetLatestAvailableSpot(DateTime.Now, UnderlyingId).ToString();
        }

        private void stockBBG_TextChanged(object sender, EventArgs e)
        {
            StockBBG = stockBBG.Text;
        }

        private void stockCompany_TextChanged(object sender, EventArgs e)
        {
            StockCompany = stockCompany.Text;
        }

        private void stockDescription_TextChanged(object sender, EventArgs e)
        {
            StockDescription = stockDescription.Text;
        }

        private void digitalRebateBox_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(digitalRebateBox.Text, out Rebate);
            digitalRebateBox.BackColor = parse ? Color.White : Color.Red;
        }

        private void barrierValueBox_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(barrierValueBox.Text, out Barrier);
            barrierValueBox.BackColor = parse ? Color.White : Color.Red;
        }

        private void barrierUpIn_CheckedChanged(object sender, EventArgs e)
        {
            if(barrierUpIn.Checked)
            {
                IsUp = true;
                IsIn = true;
            }
        }

        private void barrierUpOut_CheckedChanged(object sender, EventArgs e)
        {
            if (barrierUpOut.Checked)
            {
                IsUp = true;
                IsIn = false;
            }
        }

        private void barrierDownIn_CheckedChanged(object sender, EventArgs e)
        {
            if (barrierUpIn.Checked)
            {
                IsUp = false;
                IsIn = true;
            }
        }

        private void barrierDownOut_CheckedChanged(object sender, EventArgs e)
        {
            if (barrierUpIn.Checked)
            {
                IsUp = false;
                IsIn = false;
            }
        }
    }
}
