﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using FinanceLib.TradeManager;
using FinanceLib.FinancialCalculators;

namespace PortfolioManagerDR.GUI.Portfolio
{
    public partial class PortfolioManager : UserControl
    {
        private List<Trade> AllTrades;
        private double Vol = 0.5;

        public PortfolioManager()
        {
            InitializeComponent();
        }

        private void RefreshTrades()
        {
            allTradesGridView.Rows.Clear();
            AllTrades = TradeManager.LoadAllTrades();
            foreach (var t in AllTrades)
            {
                var i = t.Instrument;
                int col = 0;
                var row = new DataGridViewRow();
                row.CreateCells(allTradesGridView);
                row.Cells[col++].Value = t.TradeId;
                row.Cells[col++].Value = i.GetDisplayName();
                row.Cells[col++].Value = i.UnderlyingId;
                row.Cells[col++].Value = t.Direction.ToString();
                row.Cells[col++].Value = t.Quantity;
                row.Cells[col++].Value = t.TradePrice;

                allTradesGridView.Rows.Add(row);
            }

            if (AllTrades.Any())
                evaluateTradesButton.Enabled = true;

            UpdateTotalPosition();
        }

        private void UpdateTotalPosition()
        {
            var writeRow = totalPositionGridView.Rows[0];
            int wCol = 0;
            while (wCol < 6)
                writeRow.Cells[wCol++].Value = "0.0";

            for (int r = 0; r < allTradesGridView.SelectedRows.Count; r++)
            {
                var readRow = allTradesGridView.SelectedRows[r];

                int rCol = 7;
                wCol = 0;
                while (wCol < 6)
                {
                    if (!double.TryParse(Convert.ToString(writeRow.Cells[wCol].Value), out var totalVal))
                        totalVal = 0.0;
                    if (!double.TryParse(Convert.ToString(readRow.Cells[rCol++].Value), out var individualVal))
                        individualVal = 0.0;

                    writeRow.Cells[wCol++].Value = Convert.ToString(totalVal + individualVal);
                }
            }
        }


        private void refreshTradesButton_Click(object sender, EventArgs e)
        {
            RefreshTrades();
        }

        private void evaluateTradesButton_Click(object sender, EventArgs e)
        {
            var valuationDate = valuationDatePicker.Value;

            for (int r = 0; r < allTradesGridView.SelectedRows.Count; r++)
            {
                var row = allTradesGridView.SelectedRows[r];
                var t = AllTrades.Single(x => x.TradeId == Convert.ToInt32(row.Cells[0].Value));
                var i = t.Instrument;

                int col = 6;
                var currentPrice = Math.Round(i.CalcPrice(valuationDate, Vol), 2);
                row.Cells[col++].Value = currentPrice;
                row.Cells[col++].Value = Math.Round(t.Quantity * (currentPrice - t.TradePrice), 2);
                var greeks = t.CalcGreekExposure(valuationDate, Vol);
                row.Cells[col++].Value = Math.Round(greeks[Greeks.Delta], 2);
                row.Cells[col++].Value = Math.Round(greeks[Greeks.Gamma], 2);
                row.Cells[col++].Value = Math.Round(greeks[Greeks.Theta], 2);
                row.Cells[col++].Value = Math.Round(greeks[Greeks.Rho], 2);
                row.Cells[col++].Value = Math.Round(greeks[Greeks.Vega], 2);

                UpdateTotalPosition();
            }
        }

        private void volInputBox_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(volInputBox.Text, out Vol);
            volInputBox.BackColor = parse ? Color.White : Color.Red;
            evaluateTradesButton.Enabled = parse;
        }

        private void allTradesGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            UpdateTotalPosition();
        }

        private void addTradeButton_Click(object sender, EventArgs e)
        {
            var trade = new AddTradeForm(Vol);
            trade.ShowDialog();
        }

        private void deleteTradeButton_Click(object sender, EventArgs e)
        {
            for (int r = 0; r < allTradesGridView.SelectedRows.Count; r++)
            {
                var row = allTradesGridView.SelectedRows[r];
                var tradeId = (int)row.Cells[0].Value;
                TradeManager.RemoveFromDb(tradeId);
            }

            RefreshTrades();
        }

        private void addInstrumentButton_Click(object sender, EventArgs e)
        {
            var ai = new AddInstrumentForm();
            ai.ShowDialog();
        }
    }
}
