﻿namespace PortfolioManagerDR.GUI.Portfolio
{
    partial class AddInstrumentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stockInfoBox = new System.Windows.Forms.GroupBox();
            this.stockDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.stockBBG = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.stockCompany = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.optionInfoBox = new System.Windows.Forms.GroupBox();
            this.optionExpirePicker = new System.Windows.Forms.DateTimePicker();
            this.optionTypeCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.optionIsCall = new System.Windows.Forms.RadioButton();
            this.optionStrikeBox = new System.Windows.Forms.TextBox();
            this.optionIsPut = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.barrierGroupBox = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.barrierDownOut = new System.Windows.Forms.RadioButton();
            this.barrierDownIn = new System.Windows.Forms.RadioButton();
            this.barrierUpOut = new System.Windows.Forms.RadioButton();
            this.barrierUpIn = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.barrierValueBox = new System.Windows.Forms.TextBox();
            this.barrierLabel = new System.Windows.Forms.Label();
            this.digitalGroupBox = new System.Windows.Forms.GroupBox();
            this.digitalOptionLabel = new System.Windows.Forms.Label();
            this.digitalRebateBox = new System.Windows.Forms.TextBox();
            this.digitalLabel = new System.Windows.Forms.Label();
            this.instrumentTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.underlyingComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.saveInstrumentButton = new System.Windows.Forms.Button();
            this.underlyingValBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.stockInfoBox.SuspendLayout();
            this.optionInfoBox.SuspendLayout();
            this.barrierGroupBox.SuspendLayout();
            this.digitalGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // stockInfoBox
            // 
            this.stockInfoBox.Controls.Add(this.stockDescription);
            this.stockInfoBox.Controls.Add(this.label1);
            this.stockInfoBox.Controls.Add(this.stockBBG);
            this.stockInfoBox.Controls.Add(this.label8);
            this.stockInfoBox.Controls.Add(this.stockCompany);
            this.stockInfoBox.Controls.Add(this.label9);
            this.stockInfoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockInfoBox.Location = new System.Drawing.Point(0, 0);
            this.stockInfoBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.stockInfoBox.Name = "stockInfoBox";
            this.stockInfoBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.stockInfoBox.Size = new System.Drawing.Size(242, 132);
            this.stockInfoBox.TabIndex = 77;
            this.stockInfoBox.TabStop = false;
            this.stockInfoBox.Text = "StockInfo";
            this.stockInfoBox.Visible = false;
            // 
            // stockDescription
            // 
            this.stockDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockDescription.Location = new System.Drawing.Point(90, 74);
            this.stockDescription.Name = "stockDescription";
            this.stockDescription.Size = new System.Drawing.Size(134, 20);
            this.stockDescription.TabIndex = 58;
            this.stockDescription.TextChanged += new System.EventHandler(this.stockDescription_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 59;
            this.label1.Text = "Description";
            // 
            // stockBBG
            // 
            this.stockBBG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockBBG.Location = new System.Drawing.Point(90, 25);
            this.stockBBG.Name = "stockBBG";
            this.stockBBG.Size = new System.Drawing.Size(134, 20);
            this.stockBBG.TabIndex = 56;
            this.stockBBG.TextChanged += new System.EventHandler(this.stockBBG_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(4, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 57;
            this.label8.Text = "BBG Ticker";
            // 
            // stockCompany
            // 
            this.stockCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockCompany.Location = new System.Drawing.Point(90, 48);
            this.stockCompany.Name = "stockCompany";
            this.stockCompany.Size = new System.Drawing.Size(134, 20);
            this.stockCompany.TabIndex = 43;
            this.stockCompany.TextChanged += new System.EventHandler(this.stockCompany_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(4, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 52;
            this.label9.Text = "Company";
            // 
            // optionInfoBox
            // 
            this.optionInfoBox.Controls.Add(this.optionExpirePicker);
            this.optionInfoBox.Controls.Add(this.optionTypeCombo);
            this.optionInfoBox.Controls.Add(this.label16);
            this.optionInfoBox.Controls.Add(this.stockInfoBox);
            this.optionInfoBox.Controls.Add(this.optionIsCall);
            this.optionInfoBox.Controls.Add(this.optionStrikeBox);
            this.optionInfoBox.Controls.Add(this.optionIsPut);
            this.optionInfoBox.Controls.Add(this.label14);
            this.optionInfoBox.Controls.Add(this.label15);
            this.optionInfoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optionInfoBox.Location = new System.Drawing.Point(17, 100);
            this.optionInfoBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.optionInfoBox.Name = "optionInfoBox";
            this.optionInfoBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.optionInfoBox.Size = new System.Drawing.Size(241, 132);
            this.optionInfoBox.TabIndex = 76;
            this.optionInfoBox.TabStop = false;
            this.optionInfoBox.Text = "Option Info";
            this.optionInfoBox.Visible = false;
            // 
            // optionExpirePicker
            // 
            this.optionExpirePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optionExpirePicker.Location = new System.Drawing.Point(90, 80);
            this.optionExpirePicker.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.optionExpirePicker.Name = "optionExpirePicker";
            this.optionExpirePicker.Size = new System.Drawing.Size(134, 20);
            this.optionExpirePicker.TabIndex = 59;
            // 
            // optionTypeCombo
            // 
            this.optionTypeCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optionTypeCombo.FormattingEnabled = true;
            this.optionTypeCombo.Location = new System.Drawing.Point(90, 23);
            this.optionTypeCombo.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.optionTypeCombo.Name = "optionTypeCombo";
            this.optionTypeCombo.Size = new System.Drawing.Size(134, 21);
            this.optionTypeCombo.TabIndex = 58;
            this.optionTypeCombo.SelectedIndexChanged += new System.EventHandler(this.optionTypeCombo_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(4, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 57;
            this.label16.Text = "Option Type";
            // 
            // optionIsCall
            // 
            this.optionIsCall.AutoSize = true;
            this.optionIsCall.Checked = true;
            this.optionIsCall.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optionIsCall.Location = new System.Drawing.Point(183, 112);
            this.optionIsCall.Name = "optionIsCall";
            this.optionIsCall.Size = new System.Drawing.Size(42, 17);
            this.optionIsCall.TabIndex = 54;
            this.optionIsCall.TabStop = true;
            this.optionIsCall.Text = "Call";
            this.optionIsCall.UseVisualStyleBackColor = true;
            // 
            // optionStrikeBox
            // 
            this.optionStrikeBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optionStrikeBox.Location = new System.Drawing.Point(90, 51);
            this.optionStrikeBox.Name = "optionStrikeBox";
            this.optionStrikeBox.Size = new System.Drawing.Size(134, 20);
            this.optionStrikeBox.TabIndex = 43;
            this.optionStrikeBox.TextChanged += new System.EventHandler(this.optionStrikeBox_TextChanged);
            // 
            // optionIsPut
            // 
            this.optionIsPut.AutoSize = true;
            this.optionIsPut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optionIsPut.Location = new System.Drawing.Point(90, 112);
            this.optionIsPut.Name = "optionIsPut";
            this.optionIsPut.Size = new System.Drawing.Size(41, 17);
            this.optionIsPut.TabIndex = 55;
            this.optionIsPut.TabStop = true;
            this.optionIsPut.Text = "Put";
            this.optionIsPut.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(4, 52);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 52;
            this.label14.Text = "Strike";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(4, 83);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 53;
            this.label15.Text = "Expiration Date";
            // 
            // barrierGroupBox
            // 
            this.barrierGroupBox.Controls.Add(this.label13);
            this.barrierGroupBox.Controls.Add(this.label12);
            this.barrierGroupBox.Controls.Add(this.barrierDownOut);
            this.barrierGroupBox.Controls.Add(this.barrierDownIn);
            this.barrierGroupBox.Controls.Add(this.barrierUpOut);
            this.barrierGroupBox.Controls.Add(this.barrierUpIn);
            this.barrierGroupBox.Controls.Add(this.label11);
            this.barrierGroupBox.Controls.Add(this.barrierValueBox);
            this.barrierGroupBox.Controls.Add(this.barrierLabel);
            this.barrierGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrierGroupBox.Location = new System.Drawing.Point(17, 235);
            this.barrierGroupBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.barrierGroupBox.Name = "barrierGroupBox";
            this.barrierGroupBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.barrierGroupBox.Size = new System.Drawing.Size(417, 99);
            this.barrierGroupBox.TabIndex = 75;
            this.barrierGroupBox.TabStop = false;
            this.barrierGroupBox.Text = "Exotic Option Info";
            this.barrierGroupBox.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(264, 14);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Spot Must Decrease";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(148, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Spot Must Increase";
            // 
            // barrierDownOut
            // 
            this.barrierDownOut.AutoSize = true;
            this.barrierDownOut.Location = new System.Drawing.Point(266, 56);
            this.barrierDownOut.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.barrierDownOut.Name = "barrierDownOut";
            this.barrierDownOut.Size = new System.Drawing.Size(106, 17);
            this.barrierDownOut.TabIndex = 20;
            this.barrierDownOut.Text = "Down and Out";
            this.barrierDownOut.UseVisualStyleBackColor = true;
            this.barrierDownOut.CheckedChanged += new System.EventHandler(this.barrierDownOut_CheckedChanged);
            // 
            // barrierDownIn
            // 
            this.barrierDownIn.AutoSize = true;
            this.barrierDownIn.Location = new System.Drawing.Point(266, 35);
            this.barrierDownIn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.barrierDownIn.Name = "barrierDownIn";
            this.barrierDownIn.Size = new System.Drawing.Size(97, 17);
            this.barrierDownIn.TabIndex = 19;
            this.barrierDownIn.Text = "Down and In";
            this.barrierDownIn.UseVisualStyleBackColor = true;
            this.barrierDownIn.CheckedChanged += new System.EventHandler(this.barrierDownIn_CheckedChanged);
            // 
            // barrierUpOut
            // 
            this.barrierUpOut.AutoSize = true;
            this.barrierUpOut.Location = new System.Drawing.Point(151, 58);
            this.barrierUpOut.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.barrierUpOut.Name = "barrierUpOut";
            this.barrierUpOut.Size = new System.Drawing.Size(90, 17);
            this.barrierUpOut.TabIndex = 18;
            this.barrierUpOut.Text = "Up and Out";
            this.barrierUpOut.UseVisualStyleBackColor = true;
            this.barrierUpOut.CheckedChanged += new System.EventHandler(this.barrierUpOut_CheckedChanged);
            // 
            // barrierUpIn
            // 
            this.barrierUpIn.AutoSize = true;
            this.barrierUpIn.Location = new System.Drawing.Point(151, 33);
            this.barrierUpIn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.barrierUpIn.Name = "barrierUpIn";
            this.barrierUpIn.Size = new System.Drawing.Size(81, 17);
            this.barrierUpIn.TabIndex = 17;
            this.barrierUpIn.Text = "Up and In";
            this.barrierUpIn.UseVisualStyleBackColor = true;
            this.barrierUpIn.CheckedChanged += new System.EventHandler(this.barrierUpIn_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(4, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 26);
            this.label11.TabIndex = 16;
            this.label11.Text = "Barrier sets spot level at \r\nwhich payoff changes";
            // 
            // barrierValueBox
            // 
            this.barrierValueBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrierValueBox.Location = new System.Drawing.Point(48, 57);
            this.barrierValueBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.barrierValueBox.Name = "barrierValueBox";
            this.barrierValueBox.Size = new System.Drawing.Size(86, 19);
            this.barrierValueBox.TabIndex = 15;
            this.barrierValueBox.TextChanged += new System.EventHandler(this.barrierValueBox_TextChanged);
            // 
            // barrierLabel
            // 
            this.barrierLabel.AutoSize = true;
            this.barrierLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrierLabel.Location = new System.Drawing.Point(4, 59);
            this.barrierLabel.Name = "barrierLabel";
            this.barrierLabel.Size = new System.Drawing.Size(37, 13);
            this.barrierLabel.TabIndex = 11;
            this.barrierLabel.Text = "Barrier";
            // 
            // digitalGroupBox
            // 
            this.digitalGroupBox.Controls.Add(this.digitalOptionLabel);
            this.digitalGroupBox.Controls.Add(this.digitalRebateBox);
            this.digitalGroupBox.Controls.Add(this.digitalLabel);
            this.digitalGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalGroupBox.Location = new System.Drawing.Point(17, 235);
            this.digitalGroupBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.digitalGroupBox.Name = "digitalGroupBox";
            this.digitalGroupBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.digitalGroupBox.Size = new System.Drawing.Size(417, 99);
            this.digitalGroupBox.TabIndex = 40;
            this.digitalGroupBox.TabStop = false;
            this.digitalGroupBox.Text = "Exotic Option Info";
            this.digitalGroupBox.Visible = false;
            // 
            // digitalOptionLabel
            // 
            this.digitalOptionLabel.AutoSize = true;
            this.digitalOptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalOptionLabel.Location = new System.Drawing.Point(5, 14);
            this.digitalOptionLabel.Name = "digitalOptionLabel";
            this.digitalOptionLabel.Size = new System.Drawing.Size(199, 13);
            this.digitalOptionLabel.TabIndex = 16;
            this.digitalOptionLabel.Text = "Rebate Paid if Option Ends in the Money";
            // 
            // digitalRebateBox
            // 
            this.digitalRebateBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalRebateBox.Location = new System.Drawing.Point(90, 34);
            this.digitalRebateBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.digitalRebateBox.Name = "digitalRebateBox";
            this.digitalRebateBox.Size = new System.Drawing.Size(86, 20);
            this.digitalRebateBox.TabIndex = 15;
            this.digitalRebateBox.TextChanged += new System.EventHandler(this.digitalRebateBox_TextChanged);
            // 
            // digitalLabel
            // 
            this.digitalLabel.AutoSize = true;
            this.digitalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalLabel.Location = new System.Drawing.Point(5, 36);
            this.digitalLabel.Name = "digitalLabel";
            this.digitalLabel.Size = new System.Drawing.Size(42, 13);
            this.digitalLabel.TabIndex = 11;
            this.digitalLabel.Text = "Rebate";
            // 
            // instrumentTypeComboBox
            // 
            this.instrumentTypeComboBox.FormattingEnabled = true;
            this.instrumentTypeComboBox.Location = new System.Drawing.Point(113, 44);
            this.instrumentTypeComboBox.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.instrumentTypeComboBox.Name = "instrumentTypeComboBox";
            this.instrumentTypeComboBox.Size = new System.Drawing.Size(134, 21);
            this.instrumentTypeComboBox.TabIndex = 68;
            this.instrumentTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.instrumentTypeComboBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 47);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 65;
            this.label3.Text = "Instrument Type";
            // 
            // underlyingComboBox
            // 
            this.underlyingComboBox.FormattingEnabled = true;
            this.underlyingComboBox.Location = new System.Drawing.Point(113, 73);
            this.underlyingComboBox.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.underlyingComboBox.Name = "underlyingComboBox";
            this.underlyingComboBox.Size = new System.Drawing.Size(134, 21);
            this.underlyingComboBox.TabIndex = 79;
            this.underlyingComboBox.SelectedIndexChanged += new System.EventHandler(this.underlyingComboBox_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 76);
            this.label10.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 78;
            this.label10.Text = "Underlying Id";
            // 
            // saveInstrumentButton
            // 
            this.saveInstrumentButton.Location = new System.Drawing.Point(262, 193);
            this.saveInstrumentButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.saveInstrumentButton.Name = "saveInstrumentButton";
            this.saveInstrumentButton.Size = new System.Drawing.Size(100, 39);
            this.saveInstrumentButton.TabIndex = 80;
            this.saveInstrumentButton.Text = "Save Instrument to Db";
            this.saveInstrumentButton.UseVisualStyleBackColor = true;
            this.saveInstrumentButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // underlyingValBox
            // 
            this.underlyingValBox.Location = new System.Drawing.Point(262, 73);
            this.underlyingValBox.Name = "underlyingValBox";
            this.underlyingValBox.ReadOnly = true;
            this.underlyingValBox.Size = new System.Drawing.Size(118, 20);
            this.underlyingValBox.TabIndex = 81;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(262, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 82;
            this.label2.Text = "Current Underlying Level";
            // 
            // AddInstrumentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 603);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.underlyingValBox);
            this.Controls.Add(this.saveInstrumentButton);
            this.Controls.Add(this.underlyingComboBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.digitalGroupBox);
            this.Controls.Add(this.optionInfoBox);
            this.Controls.Add(this.barrierGroupBox);
            this.Controls.Add(this.instrumentTypeComboBox);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.Name = "AddInstrumentForm";
            this.Text = "AddInstrumentForm";
            this.stockInfoBox.ResumeLayout(false);
            this.stockInfoBox.PerformLayout();
            this.optionInfoBox.ResumeLayout(false);
            this.optionInfoBox.PerformLayout();
            this.barrierGroupBox.ResumeLayout(false);
            this.barrierGroupBox.PerformLayout();
            this.digitalGroupBox.ResumeLayout(false);
            this.digitalGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox stockInfoBox;
        private System.Windows.Forms.TextBox stockBBG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox stockCompany;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox optionInfoBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RadioButton optionIsCall;
        private System.Windows.Forms.RadioButton optionIsPut;
        private System.Windows.Forms.TextBox optionStrikeBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox barrierGroupBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox digitalGroupBox;
        private System.Windows.Forms.Label digitalOptionLabel;
        private System.Windows.Forms.TextBox digitalRebateBox;
        private System.Windows.Forms.Label digitalLabel;
        private System.Windows.Forms.RadioButton barrierDownOut;
        private System.Windows.Forms.RadioButton barrierDownIn;
        private System.Windows.Forms.RadioButton barrierUpOut;
        private System.Windows.Forms.RadioButton barrierUpIn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox barrierValueBox;
        private System.Windows.Forms.Label barrierLabel;
        private System.Windows.Forms.ComboBox instrumentTypeComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox underlyingComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox optionTypeCombo;
        private System.Windows.Forms.DateTimePicker optionExpirePicker;
        private System.Windows.Forms.TextBox stockDescription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button saveInstrumentButton;
        private System.Windows.Forms.TextBox underlyingValBox;
        private System.Windows.Forms.Label label2;
    }
}