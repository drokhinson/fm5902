﻿namespace PortfolioManagerDR.GUI.Portfolio
{
    partial class PortfolioManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.allTradesGridView = new System.Windows.Forms.DataGridView();
            this.TradeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InstrumentType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Underlying = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TradedPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PnL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gamma = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Theta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rho = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vega = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addTradeButton = new System.Windows.Forms.Button();
            this.deleteTradeButton = new System.Windows.Forms.Button();
            this.addInstrumentButton = new System.Windows.Forms.Button();
            this.refreshTradesButton = new System.Windows.Forms.Button();
            this.valuationDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.volInputBox = new System.Windows.Forms.TextBox();
            this.evaluateTradesButton = new System.Windows.Forms.Button();
            this.totalPositionGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.allTradesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalPositionGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // allTradesGridView
            // 
            this.allTradesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allTradesGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TradeId,
            this.InstrumentType,
            this.Underlying,
            this.Direction,
            this.Quantity,
            this.TradedPrice,
            this.CurrentPrice,
            this.PnL,
            this.Delta,
            this.Gamma,
            this.Theta,
            this.Rho,
            this.Vega});
            this.allTradesGridView.Location = new System.Drawing.Point(0, 641);
            this.allTradesGridView.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.allTradesGridView.Name = "allTradesGridView";
            this.allTradesGridView.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.allTradesGridView.Size = new System.Drawing.Size(2453, 594);
            this.allTradesGridView.TabIndex = 0;
            this.allTradesGridView.TabStop = false;
            this.allTradesGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.allTradesGridView_RowHeaderMouseClick);
            // 
            // TradeId
            // 
            this.TradeId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TradeId.HeaderText = "Trade Id";
            this.TradeId.Name = "TradeId";
            this.TradeId.ReadOnly = true;
            // 
            // InstrumentType
            // 
            this.InstrumentType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.InstrumentType.HeaderText = "Instrument Type";
            this.InstrumentType.Name = "InstrumentType";
            this.InstrumentType.ReadOnly = true;
            this.InstrumentType.Width = 250;
            // 
            // Underlying
            // 
            this.Underlying.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Underlying.HeaderText = "Underlying";
            this.Underlying.Name = "Underlying";
            this.Underlying.ReadOnly = true;
            // 
            // Direction
            // 
            this.Direction.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Direction.HeaderText = "Direction";
            this.Direction.Name = "Direction";
            this.Direction.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // TradedPrice
            // 
            this.TradedPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TradedPrice.HeaderText = "Traded Price";
            this.TradedPrice.Name = "TradedPrice";
            this.TradedPrice.ReadOnly = true;
            // 
            // CurrentPrice
            // 
            this.CurrentPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CurrentPrice.HeaderText = "Current Price";
            this.CurrentPrice.Name = "CurrentPrice";
            this.CurrentPrice.ReadOnly = true;
            // 
            // PnL
            // 
            this.PnL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PnL.HeaderText = "P&L";
            this.PnL.Name = "PnL";
            this.PnL.ReadOnly = true;
            // 
            // Delta
            // 
            this.Delta.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Delta.HeaderText = "Delta";
            this.Delta.Name = "Delta";
            this.Delta.ReadOnly = true;
            // 
            // Gamma
            // 
            this.Gamma.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Gamma.HeaderText = "Gamma";
            this.Gamma.Name = "Gamma";
            this.Gamma.ReadOnly = true;
            // 
            // Theta
            // 
            this.Theta.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Theta.HeaderText = "Theta";
            this.Theta.Name = "Theta";
            this.Theta.ReadOnly = true;
            // 
            // Rho
            // 
            this.Rho.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Rho.HeaderText = "Rho";
            this.Rho.Name = "Rho";
            this.Rho.ReadOnly = true;
            // 
            // Vega
            // 
            this.Vega.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Vega.HeaderText = "Vega";
            this.Vega.Name = "Vega";
            this.Vega.ReadOnly = true;
            // 
            // addTradeButton
            // 
            this.addTradeButton.Location = new System.Drawing.Point(1857, 495);
            this.addTradeButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.addTradeButton.Name = "addTradeButton";
            this.addTradeButton.Size = new System.Drawing.Size(243, 55);
            this.addTradeButton.TabIndex = 1;
            this.addTradeButton.Text = "Add Trade";
            this.addTradeButton.UseVisualStyleBackColor = true;
            this.addTradeButton.Click += new System.EventHandler(this.addTradeButton_Click);
            // 
            // deleteTradeButton
            // 
            this.deleteTradeButton.Location = new System.Drawing.Point(1857, 564);
            this.deleteTradeButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.deleteTradeButton.Name = "deleteTradeButton";
            this.deleteTradeButton.Size = new System.Drawing.Size(243, 55);
            this.deleteTradeButton.TabIndex = 2;
            this.deleteTradeButton.Text = "Delete Trade";
            this.deleteTradeButton.UseVisualStyleBackColor = true;
            this.deleteTradeButton.Click += new System.EventHandler(this.deleteTradeButton_Click);
            // 
            // addInstrumentButton
            // 
            this.addInstrumentButton.Location = new System.Drawing.Point(2116, 495);
            this.addInstrumentButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.addInstrumentButton.Name = "addInstrumentButton";
            this.addInstrumentButton.Size = new System.Drawing.Size(285, 55);
            this.addInstrumentButton.TabIndex = 3;
            this.addInstrumentButton.Text = "Add Instrument";
            this.addInstrumentButton.UseVisualStyleBackColor = true;
            this.addInstrumentButton.Click += new System.EventHandler(this.addInstrumentButton_Click);
            // 
            // refreshTradesButton
            // 
            this.refreshTradesButton.Location = new System.Drawing.Point(17, 158);
            this.refreshTradesButton.Name = "refreshTradesButton";
            this.refreshTradesButton.Size = new System.Drawing.Size(298, 124);
            this.refreshTradesButton.TabIndex = 4;
            this.refreshTradesButton.Text = "Refresh Trades";
            this.refreshTradesButton.UseVisualStyleBackColor = true;
            this.refreshTradesButton.Click += new System.EventHandler(this.refreshTradesButton_Click);
            // 
            // valuationDatePicker
            // 
            this.valuationDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.valuationDatePicker.Location = new System.Drawing.Point(17, 439);
            this.valuationDatePicker.Name = "valuationDatePicker";
            this.valuationDatePicker.Size = new System.Drawing.Size(234, 38);
            this.valuationDatePicker.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 295);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 32);
            this.label1.TabIndex = 6;
            this.label1.Text = "Market Volatility";
            // 
            // volInputBox
            // 
            this.volInputBox.Location = new System.Drawing.Point(17, 341);
            this.volInputBox.Name = "volInputBox";
            this.volInputBox.Size = new System.Drawing.Size(211, 38);
            this.volInputBox.TabIndex = 7;
            this.volInputBox.Text = "0.5";
            this.volInputBox.TextChanged += new System.EventHandler(this.volInputBox_TextChanged);
            // 
            // evaluateTradesButton
            // 
            this.evaluateTradesButton.Enabled = false;
            this.evaluateTradesButton.Location = new System.Drawing.Point(17, 495);
            this.evaluateTradesButton.Name = "evaluateTradesButton";
            this.evaluateTradesButton.Size = new System.Drawing.Size(298, 124);
            this.evaluateTradesButton.TabIndex = 8;
            this.evaluateTradesButton.Text = "Evaluate Trades";
            this.evaluateTradesButton.UseVisualStyleBackColor = true;
            this.evaluateTradesButton.Click += new System.EventHandler(this.evaluateTradesButton_Click);
            // 
            // totalPositionGridView
            // 
            this.totalPositionGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.totalPositionGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.totalPositionGridView.Location = new System.Drawing.Point(339, 481);
            this.totalPositionGridView.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.totalPositionGridView.Name = "totalPositionGridView";
            this.totalPositionGridView.Size = new System.Drawing.Size(1479, 138);
            this.totalPositionGridView.TabIndex = 9;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn8.HeaderText = "Total P&L";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.HeaderText = "Total Delta";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn10.HeaderText = "Total Gamma";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn11.HeaderText = "Total Theta";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn12.HeaderText = "Total Rho";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn13.HeaderText = "Total Vega";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 393);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(203, 32);
            this.label2.TabIndex = 10;
            this.label2.Text = "Valuation Date";
            // 
            // PortfolioManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.totalPositionGridView);
            this.Controls.Add(this.evaluateTradesButton);
            this.Controls.Add(this.volInputBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.valuationDatePicker);
            this.Controls.Add(this.refreshTradesButton);
            this.Controls.Add(this.addInstrumentButton);
            this.Controls.Add(this.deleteTradeButton);
            this.Controls.Add(this.addTradeButton);
            this.Controls.Add(this.allTradesGridView);
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "PortfolioManager";
            this.Size = new System.Drawing.Size(2453, 1235);
            ((System.ComponentModel.ISupportInitialize)(this.allTradesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalPositionGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView allTradesGridView;
        private System.Windows.Forms.Button addTradeButton;
        private System.Windows.Forms.Button deleteTradeButton;
        private System.Windows.Forms.Button addInstrumentButton;
        private System.Windows.Forms.Button refreshTradesButton;
        private System.Windows.Forms.DateTimePicker valuationDatePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox volInputBox;
        private System.Windows.Forms.Button evaluateTradesButton;
        private System.Windows.Forms.DataGridView totalPositionGridView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn TradeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn InstrumentType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Underlying;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direction;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn TradedPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn PnL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Delta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gamma;
        private System.Windows.Forms.DataGridViewTextBoxColumn Theta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rho;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vega;
    }
}
