﻿using Database;
using FinanceLib.Instruments;
using FinanceLib.TradeManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortfolioManagerDR.GUI.Portfolio
{
    public partial class AddTradeForm : Form
    {
        private int TradeId;
        private double TradePrice;
        private int TradeQuantity;
        private bool IsBuy = true;

        private bool InputT = false;
        private bool InputQ = false;

        private Instrument SelectedInst;
        private int InstrumentId = -1;
        private List<Instrument> AllInstruments;
        private double Vol;

        public AddTradeForm(double vol)
        {
            Vol = vol;
            InitializeComponent();
            GetTradeId();
            PopulateInstruments();
        }

        private void GetTradeId()
        {
            var db = new PortfolioDB();
            if (!db.Portfolio_Trades.Any())
                TradeId = 1;
            else
                TradeId = db.Portfolio_Trades.Max(r => r.TradeId) + 1;

            tradeIdBox.Text = TradeId.ToString();
        }

        private void CheckInputs()
        {
            saveToDbButton.Enabled = InputT && InstrumentId > 0 && InputQ;
        }

        private void PopulateInstruments()
        {
            instrumentComboBox.Items.Clear();
            instrumentComboBox.Items.Add("Add New...");
            AllInstruments = InstrumentUtils.LoadAllInstruments();
            instrumentComboBox.Items.AddRange(AllInstruments.ToArray());
        }

        private void saveToDbButton_Click(object sender, EventArgs e)
        {
            var tradeInfo = new Portfolio_Trades
            {
                TradeId = TradeId,
                TradeDateTime = effectiveDatePicker.Value,
                TradePrice = TradePrice,
                TradeQuantity = TradeQuantity,
                IsBuy = IsBuy,
                InstrumentId = InstrumentId
            };

            var db = new PortfolioDB();
            db.Portfolio_Trades.Add(tradeInfo);
            db.SaveChanges();

            string message = "New Trade saved to DB. TradeId: " + TradeId;
            MessageBox.Show(message, "Saved", MessageBoxButtons.OK);
            GetTradeId();
        }

        private void tradedPriceBox_TextChanged(object sender, EventArgs e)
        {
            var parse = double.TryParse(tradedPriceBox.Text, out TradePrice);
            tradedPriceBox.BackColor = parse ? Color.White : Color.Red;
            InputT = parse;
            CheckInputs();
        }

        private void quantityBox_TextChanged(object sender, EventArgs e)
        {
            var parse = int.TryParse(quantityBox.Text, out TradeQuantity);
            quantityBox.BackColor = parse ? Color.White : Color.Red;
            InputQ = parse;
            CheckInputs();
        }

        private void instrumentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            stockInfoBox.Visible = false;
            optionInfoBox.Visible = false;
            digitalGroupBox.Visible = false;
            barrierGroupBox.Visible = false;

            if (instrumentComboBox.SelectedIndex != 0)
            {
                SelectedInst = (Instrument)instrumentComboBox.SelectedItem;
                InstrumentId = SelectedInst.InstrumentId;
                UpdateView();
            }
            else if (instrumentComboBox.SelectedIndex == 0)
            {
                var ai = new AddInstrumentForm();
                ai.ShowDialog();
                PopulateInstruments();
            }
            else
                InstrumentId = -1;

            CheckInputs();
        }

        private void UpdateView()
        {
            var instType = SelectedInst.GetInstrumentType();
            switch(instType)
            {
                case (InstrumentType.Stock):
                    var s = (Stock)SelectedInst;
                    stockInfoBox.Visible = true;
                    stockBBG.Text = s.BloombergTicker;
                    stockId.Text = s.InstrumentId.ToString();
                    stockCompany.Text = s.CompanyName;
                    break;
                case (InstrumentType.Option):
                    optionInfoBox.Visible = true;
                    var o = (Option)SelectedInst;
                    optionTypeBox.Text = o.GetOptionType().ToString();
                    optionExpirationBox.Text = o.ExpirationDate.ToShortDateString();
                    optionIdBox.Text = o.InstrumentId.ToString();
                    optionIsCall.Checked = o.IsCall;
                    optionIsPut.Checked = !o.IsCall;
                    optionStrikeBox.Text = o.K.ToString();
                    if (o.GetOptionType() == OptionType.Digital) {
                        digitalGroupBox.Visible = true;
                        var d = (DigitalOption)o;
                        digitalRebateBox.Text = d.Rebate.ToString();
                    }
                    if (o.GetOptionType() == OptionType.Barrier) {
                        barrierGroupBox.Visible = true;
                        var b = (BarrierOption)o;
                        barrierValueBox.Text = b.Barrier.ToString();
                        barrierUpIn.Checked = b.IsUp && b.IsIn;
                        barrierUpOut.Checked = b.IsUp && !b.IsIn;
                        barrierDownOut.Checked = !b.IsUp && !b.IsIn;
                        barrierDownIn.Checked = !b.IsUp && b.IsIn;
                    }
                    break;
            }
        }

        private void buyRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            IsBuy = buyRadioButton.Checked;
        }

        private void evalPriceButton_Click(object sender, EventArgs e)
        {
            if (InstrumentId > 0)
                tradedPriceBox.Text = SelectedInst.CalcPrice(DateTime.Now, Vol).ToString();
        }
    }
}
