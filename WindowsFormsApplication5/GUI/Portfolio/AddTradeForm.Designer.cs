﻿namespace PortfolioManagerDR.GUI.Portfolio
{
    partial class AddTradeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tradedPriceBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.effectiveDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.tradeIdBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.instrumentComboBox = new System.Windows.Forms.ComboBox();
            this.quantityBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.sellRadioButton = new System.Windows.Forms.RadioButton();
            this.buyRadioButton = new System.Windows.Forms.RadioButton();
            this.saveToDbButton = new System.Windows.Forms.Button();
            this.barrierGroupBox = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.digitalGroupBox = new System.Windows.Forms.GroupBox();
            this.digitalOptionLabel = new System.Windows.Forms.Label();
            this.digitalRebateBox = new System.Windows.Forms.TextBox();
            this.digitalLabel = new System.Windows.Forms.Label();
            this.barrierDownOut = new System.Windows.Forms.RadioButton();
            this.barrierDownIn = new System.Windows.Forms.RadioButton();
            this.barrierUpOut = new System.Windows.Forms.RadioButton();
            this.barrierUpIn = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.barrierValueBox = new System.Windows.Forms.TextBox();
            this.barrierLabel = new System.Windows.Forms.Label();
            this.optionExpirationBox = new System.Windows.Forms.TextBox();
            this.optionStrikeBox = new System.Windows.Forms.TextBox();
            this.optionIsCall = new System.Windows.Forms.RadioButton();
            this.optionIsPut = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.optionInfoBox = new System.Windows.Forms.GroupBox();
            this.optionIdBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.optionTypeBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.stockInfoBox = new System.Windows.Forms.GroupBox();
            this.stockId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.stockBBG = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.stockCompany = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.evalPriceButton = new System.Windows.Forms.Button();
            this.barrierGroupBox.SuspendLayout();
            this.digitalGroupBox.SuspendLayout();
            this.optionInfoBox.SuspendLayout();
            this.stockInfoBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tradedPriceBox
            // 
            this.tradedPriceBox.Location = new System.Drawing.Point(263, 303);
            this.tradedPriceBox.Name = "tradedPriceBox";
            this.tradedPriceBox.Size = new System.Drawing.Size(350, 38);
            this.tradedPriceBox.TabIndex = 32;
            this.tradedPriceBox.TextChanged += new System.EventHandler(this.tradedPriceBox_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 306);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(177, 32);
            this.label4.TabIndex = 31;
            this.label4.Text = "Traded Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(78, 232);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 32);
            this.label3.TabIndex = 29;
            this.label3.Text = "Instrument";
            // 
            // effectiveDatePicker
            // 
            this.effectiveDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.effectiveDatePicker.Location = new System.Drawing.Point(263, 155);
            this.effectiveDatePicker.Name = "effectiveDatePicker";
            this.effectiveDatePicker.Size = new System.Drawing.Size(350, 38);
            this.effectiveDatePicker.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 32);
            this.label2.TabIndex = 27;
            this.label2.Text = "Trade Date";
            // 
            // tradeIdBox
            // 
            this.tradeIdBox.Enabled = false;
            this.tradeIdBox.Location = new System.Drawing.Point(263, 79);
            this.tradeIdBox.Name = "tradeIdBox";
            this.tradeIdBox.Size = new System.Drawing.Size(350, 38);
            this.tradeIdBox.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 32);
            this.label1.TabIndex = 25;
            this.label1.Text = "TradeId";
            // 
            // instrumentComboBox
            // 
            this.instrumentComboBox.FormattingEnabled = true;
            this.instrumentComboBox.Location = new System.Drawing.Point(263, 229);
            this.instrumentComboBox.Name = "instrumentComboBox";
            this.instrumentComboBox.Size = new System.Drawing.Size(350, 39);
            this.instrumentComboBox.TabIndex = 33;
            this.instrumentComboBox.SelectedIndexChanged += new System.EventHandler(this.instrumentComboBox_SelectedIndexChanged);
            // 
            // quantityBox
            // 
            this.quantityBox.Location = new System.Drawing.Point(263, 380);
            this.quantityBox.Name = "quantityBox";
            this.quantityBox.Size = new System.Drawing.Size(350, 38);
            this.quantityBox.TabIndex = 35;
            this.quantityBox.TextChanged += new System.EventHandler(this.quantityBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(104, 383);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 32);
            this.label5.TabIndex = 34;
            this.label5.Text = "Quantity";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(104, 460);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 32);
            this.label6.TabIndex = 36;
            this.label6.Text = "Direction";
            // 
            // sellRadioButton
            // 
            this.sellRadioButton.AutoSize = true;
            this.sellRadioButton.Location = new System.Drawing.Point(491, 457);
            this.sellRadioButton.Name = "sellRadioButton";
            this.sellRadioButton.Size = new System.Drawing.Size(122, 36);
            this.sellRadioButton.TabIndex = 37;
            this.sellRadioButton.TabStop = true;
            this.sellRadioButton.Text = "SELL";
            this.sellRadioButton.UseVisualStyleBackColor = true;
            // 
            // buyRadioButton
            // 
            this.buyRadioButton.AutoSize = true;
            this.buyRadioButton.Checked = true;
            this.buyRadioButton.Location = new System.Drawing.Point(263, 460);
            this.buyRadioButton.Name = "buyRadioButton";
            this.buyRadioButton.Size = new System.Drawing.Size(110, 36);
            this.buyRadioButton.TabIndex = 37;
            this.buyRadioButton.TabStop = true;
            this.buyRadioButton.Text = "BUY";
            this.buyRadioButton.UseVisualStyleBackColor = true;
            this.buyRadioButton.CheckedChanged += new System.EventHandler(this.buyRadioButton_CheckedChanged);
            // 
            // saveToDbButton
            // 
            this.saveToDbButton.Enabled = false;
            this.saveToDbButton.Location = new System.Drawing.Point(263, 542);
            this.saveToDbButton.Name = "saveToDbButton";
            this.saveToDbButton.Size = new System.Drawing.Size(350, 97);
            this.saveToDbButton.TabIndex = 38;
            this.saveToDbButton.Text = "Save To Db";
            this.saveToDbButton.UseVisualStyleBackColor = true;
            this.saveToDbButton.Click += new System.EventHandler(this.saveToDbButton_Click);
            // 
            // barrierGroupBox
            // 
            this.barrierGroupBox.Controls.Add(this.label13);
            this.barrierGroupBox.Controls.Add(this.label12);
            this.barrierGroupBox.Controls.Add(this.digitalGroupBox);
            this.barrierGroupBox.Controls.Add(this.barrierDownOut);
            this.barrierGroupBox.Controls.Add(this.barrierDownIn);
            this.barrierGroupBox.Controls.Add(this.barrierUpOut);
            this.barrierGroupBox.Controls.Add(this.barrierUpIn);
            this.barrierGroupBox.Controls.Add(this.label11);
            this.barrierGroupBox.Controls.Add(this.barrierValueBox);
            this.barrierGroupBox.Controls.Add(this.barrierLabel);
            this.barrierGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrierGroupBox.Location = new System.Drawing.Point(153, 1027);
            this.barrierGroupBox.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.barrierGroupBox.Name = "barrierGroupBox";
            this.barrierGroupBox.Padding = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.barrierGroupBox.Size = new System.Drawing.Size(1112, 236);
            this.barrierGroupBox.TabIndex = 41;
            this.barrierGroupBox.TabStop = false;
            this.barrierGroupBox.Text = "Exotic Option Info";
            this.barrierGroupBox.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(704, 33);
            this.label13.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(260, 31);
            this.label13.TabIndex = 22;
            this.label13.Text = "Spot Must Decrease";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(396, 33);
            this.label12.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(248, 31);
            this.label12.TabIndex = 21;
            this.label12.Text = "Spot Must Increase";
            // 
            // digitalGroupBox
            // 
            this.digitalGroupBox.Controls.Add(this.digitalOptionLabel);
            this.digitalGroupBox.Controls.Add(this.digitalRebateBox);
            this.digitalGroupBox.Controls.Add(this.digitalLabel);
            this.digitalGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalGroupBox.Location = new System.Drawing.Point(0, 0);
            this.digitalGroupBox.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.digitalGroupBox.Name = "digitalGroupBox";
            this.digitalGroupBox.Padding = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.digitalGroupBox.Size = new System.Drawing.Size(1112, 236);
            this.digitalGroupBox.TabIndex = 40;
            this.digitalGroupBox.TabStop = false;
            this.digitalGroupBox.Text = "Exotic Option Info";
            this.digitalGroupBox.Visible = false;
            // 
            // digitalOptionLabel
            // 
            this.digitalOptionLabel.AutoSize = true;
            this.digitalOptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalOptionLabel.Location = new System.Drawing.Point(14, 33);
            this.digitalOptionLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.digitalOptionLabel.Name = "digitalOptionLabel";
            this.digitalOptionLabel.Size = new System.Drawing.Size(501, 31);
            this.digitalOptionLabel.TabIndex = 16;
            this.digitalOptionLabel.Text = "Rebate Paid if Option Ends in the Money";
            // 
            // digitalRebateBox
            // 
            this.digitalRebateBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalRebateBox.Location = new System.Drawing.Point(240, 81);
            this.digitalRebateBox.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.digitalRebateBox.Name = "digitalRebateBox";
            this.digitalRebateBox.ReadOnly = true;
            this.digitalRebateBox.Size = new System.Drawing.Size(222, 37);
            this.digitalRebateBox.TabIndex = 15;
            // 
            // digitalLabel
            // 
            this.digitalLabel.AutoSize = true;
            this.digitalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.digitalLabel.Location = new System.Drawing.Point(14, 87);
            this.digitalLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.digitalLabel.Name = "digitalLabel";
            this.digitalLabel.Size = new System.Drawing.Size(102, 31);
            this.digitalLabel.TabIndex = 11;
            this.digitalLabel.Text = "Rebate";
            // 
            // barrierDownOut
            // 
            this.barrierDownOut.AutoSize = true;
            this.barrierDownOut.Enabled = false;
            this.barrierDownOut.Location = new System.Drawing.Point(710, 134);
            this.barrierDownOut.Margin = new System.Windows.Forms.Padding(6);
            this.barrierDownOut.Name = "barrierDownOut";
            this.barrierDownOut.Size = new System.Drawing.Size(236, 35);
            this.barrierDownOut.TabIndex = 20;
            this.barrierDownOut.TabStop = true;
            this.barrierDownOut.Text = "Down and Out";
            this.barrierDownOut.UseVisualStyleBackColor = true;
            // 
            // barrierDownIn
            // 
            this.barrierDownIn.AutoSize = true;
            this.barrierDownIn.Enabled = false;
            this.barrierDownIn.Location = new System.Drawing.Point(710, 83);
            this.barrierDownIn.Margin = new System.Windows.Forms.Padding(6);
            this.barrierDownIn.Name = "barrierDownIn";
            this.barrierDownIn.Size = new System.Drawing.Size(214, 35);
            this.barrierDownIn.TabIndex = 19;
            this.barrierDownIn.TabStop = true;
            this.barrierDownIn.Text = "Down and In";
            this.barrierDownIn.UseVisualStyleBackColor = true;
            // 
            // barrierUpOut
            // 
            this.barrierUpOut.AutoSize = true;
            this.barrierUpOut.Enabled = false;
            this.barrierUpOut.Location = new System.Drawing.Point(402, 138);
            this.barrierUpOut.Margin = new System.Windows.Forms.Padding(6);
            this.barrierUpOut.Name = "barrierUpOut";
            this.barrierUpOut.Size = new System.Drawing.Size(199, 35);
            this.barrierUpOut.TabIndex = 18;
            this.barrierUpOut.TabStop = true;
            this.barrierUpOut.Text = "Up and Out";
            this.barrierUpOut.UseVisualStyleBackColor = true;
            // 
            // barrierUpIn
            // 
            this.barrierUpIn.AutoSize = true;
            this.barrierUpIn.Enabled = false;
            this.barrierUpIn.Location = new System.Drawing.Point(402, 79);
            this.barrierUpIn.Margin = new System.Windows.Forms.Padding(6);
            this.barrierUpIn.Name = "barrierUpIn";
            this.barrierUpIn.Size = new System.Drawing.Size(177, 35);
            this.barrierUpIn.TabIndex = 17;
            this.barrierUpIn.TabStop = true;
            this.barrierUpIn.Text = "Up and In";
            this.barrierUpIn.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 54);
            this.label11.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(312, 62);
            this.label11.TabIndex = 16;
            this.label11.Text = "Barrier sets spot level at \r\nwhich payoff changes";
            // 
            // barrierValueBox
            // 
            this.barrierValueBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrierValueBox.Location = new System.Drawing.Point(128, 136);
            this.barrierValueBox.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.barrierValueBox.Name = "barrierValueBox";
            this.barrierValueBox.ReadOnly = true;
            this.barrierValueBox.Size = new System.Drawing.Size(222, 37);
            this.barrierValueBox.TabIndex = 15;
            // 
            // barrierLabel
            // 
            this.barrierLabel.AutoSize = true;
            this.barrierLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barrierLabel.Location = new System.Drawing.Point(12, 141);
            this.barrierLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.barrierLabel.Name = "barrierLabel";
            this.barrierLabel.Size = new System.Drawing.Size(95, 31);
            this.barrierLabel.TabIndex = 11;
            this.barrierLabel.Text = "Barrier";
            // 
            // optionExpirationBox
            // 
            this.optionExpirationBox.Location = new System.Drawing.Point(245, 162);
            this.optionExpirationBox.Margin = new System.Windows.Forms.Padding(8);
            this.optionExpirationBox.Name = "optionExpirationBox";
            this.optionExpirationBox.ReadOnly = true;
            this.optionExpirationBox.Size = new System.Drawing.Size(260, 37);
            this.optionExpirationBox.TabIndex = 42;
            // 
            // optionStrikeBox
            // 
            this.optionStrikeBox.Location = new System.Drawing.Point(245, 113);
            this.optionStrikeBox.Margin = new System.Windows.Forms.Padding(8);
            this.optionStrikeBox.Name = "optionStrikeBox";
            this.optionStrikeBox.ReadOnly = true;
            this.optionStrikeBox.Size = new System.Drawing.Size(260, 37);
            this.optionStrikeBox.TabIndex = 43;
            // 
            // optionIsCall
            // 
            this.optionIsCall.AutoSize = true;
            this.optionIsCall.Checked = true;
            this.optionIsCall.Enabled = false;
            this.optionIsCall.Location = new System.Drawing.Point(793, 115);
            this.optionIsCall.Margin = new System.Windows.Forms.Padding(8);
            this.optionIsCall.Name = "optionIsCall";
            this.optionIsCall.Size = new System.Drawing.Size(102, 35);
            this.optionIsCall.TabIndex = 54;
            this.optionIsCall.TabStop = true;
            this.optionIsCall.Text = "Call";
            this.optionIsCall.UseVisualStyleBackColor = true;
            // 
            // optionIsPut
            // 
            this.optionIsPut.AutoSize = true;
            this.optionIsPut.Enabled = false;
            this.optionIsPut.Location = new System.Drawing.Point(958, 115);
            this.optionIsPut.Margin = new System.Windows.Forms.Padding(8);
            this.optionIsPut.Name = "optionIsPut";
            this.optionIsPut.Size = new System.Drawing.Size(95, 35);
            this.optionIsPut.TabIndex = 55;
            this.optionIsPut.TabStop = true;
            this.optionIsPut.Text = "Put";
            this.optionIsPut.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(19, 115);
            this.label14.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 32);
            this.label14.TabIndex = 52;
            this.label14.Text = "Strike";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(19, 164);
            this.label15.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(210, 32);
            this.label15.TabIndex = 53;
            this.label15.Text = "Expiration Date";
            // 
            // optionInfoBox
            // 
            this.optionInfoBox.Controls.Add(this.optionIdBox);
            this.optionInfoBox.Controls.Add(this.label17);
            this.optionInfoBox.Controls.Add(this.optionTypeBox);
            this.optionInfoBox.Controls.Add(this.label16);
            this.optionInfoBox.Controls.Add(this.optionIsCall);
            this.optionInfoBox.Controls.Add(this.optionIsPut);
            this.optionInfoBox.Controls.Add(this.optionExpirationBox);
            this.optionInfoBox.Controls.Add(this.optionStrikeBox);
            this.optionInfoBox.Controls.Add(this.label14);
            this.optionInfoBox.Controls.Add(this.label15);
            this.optionInfoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optionInfoBox.Location = new System.Drawing.Point(151, 783);
            this.optionInfoBox.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.optionInfoBox.Name = "optionInfoBox";
            this.optionInfoBox.Padding = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.optionInfoBox.Size = new System.Drawing.Size(1112, 236);
            this.optionInfoBox.TabIndex = 57;
            this.optionInfoBox.TabStop = false;
            this.optionInfoBox.Text = "Option Info";
            this.optionInfoBox.Visible = false;
            // 
            // optionIdBox
            // 
            this.optionIdBox.Location = new System.Drawing.Point(793, 60);
            this.optionIdBox.Margin = new System.Windows.Forms.Padding(8);
            this.optionIdBox.Name = "optionIdBox";
            this.optionIdBox.ReadOnly = true;
            this.optionIdBox.Size = new System.Drawing.Size(260, 37);
            this.optionIdBox.TabIndex = 58;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(599, 62);
            this.label17.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(178, 32);
            this.label17.TabIndex = 59;
            this.label17.Text = "Instrument Id";
            // 
            // optionTypeBox
            // 
            this.optionTypeBox.Location = new System.Drawing.Point(245, 60);
            this.optionTypeBox.Margin = new System.Windows.Forms.Padding(8);
            this.optionTypeBox.Name = "optionTypeBox";
            this.optionTypeBox.ReadOnly = true;
            this.optionTypeBox.Size = new System.Drawing.Size(260, 37);
            this.optionTypeBox.TabIndex = 56;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(19, 62);
            this.label16.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(170, 32);
            this.label16.TabIndex = 57;
            this.label16.Text = "Option Type";
            // 
            // stockInfoBox
            // 
            this.stockInfoBox.Controls.Add(this.stockId);
            this.stockInfoBox.Controls.Add(this.label7);
            this.stockInfoBox.Controls.Add(this.stockBBG);
            this.stockInfoBox.Controls.Add(this.label8);
            this.stockInfoBox.Controls.Add(this.stockCompany);
            this.stockInfoBox.Controls.Add(this.label9);
            this.stockInfoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockInfoBox.Location = new System.Drawing.Point(153, 783);
            this.stockInfoBox.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.stockInfoBox.Name = "stockInfoBox";
            this.stockInfoBox.Padding = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.stockInfoBox.Size = new System.Drawing.Size(1112, 236);
            this.stockInfoBox.TabIndex = 60;
            this.stockInfoBox.TabStop = false;
            this.stockInfoBox.Text = "StockInfo";
            this.stockInfoBox.Visible = false;
            // 
            // stockId
            // 
            this.stockId.Location = new System.Drawing.Point(793, 60);
            this.stockId.Margin = new System.Windows.Forms.Padding(8);
            this.stockId.Name = "stockId";
            this.stockId.ReadOnly = true;
            this.stockId.Size = new System.Drawing.Size(260, 37);
            this.stockId.TabIndex = 58;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(599, 62);
            this.label7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(178, 32);
            this.label7.TabIndex = 59;
            this.label7.Text = "Instrument Id";
            // 
            // stockBBG
            // 
            this.stockBBG.Location = new System.Drawing.Point(245, 60);
            this.stockBBG.Margin = new System.Windows.Forms.Padding(8);
            this.stockBBG.Name = "stockBBG";
            this.stockBBG.ReadOnly = true;
            this.stockBBG.Size = new System.Drawing.Size(260, 37);
            this.stockBBG.TabIndex = 56;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(19, 62);
            this.label8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(159, 32);
            this.label8.TabIndex = 57;
            this.label8.Text = "BBG Ticker";
            // 
            // stockCompany
            // 
            this.stockCompany.Location = new System.Drawing.Point(245, 113);
            this.stockCompany.Margin = new System.Windows.Forms.Padding(8);
            this.stockCompany.Name = "stockCompany";
            this.stockCompany.ReadOnly = true;
            this.stockCompany.Size = new System.Drawing.Size(260, 37);
            this.stockCompany.TabIndex = 43;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(19, 115);
            this.label9.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 32);
            this.label9.TabIndex = 52;
            this.label9.Text = "Company";
            // 
            // evalPriceButton
            // 
            this.evalPriceButton.Location = new System.Drawing.Point(620, 303);
            this.evalPriceButton.Name = "evalPriceButton";
            this.evalPriceButton.Size = new System.Drawing.Size(204, 38);
            this.evalPriceButton.TabIndex = 61;
            this.evalPriceButton.Text = "Eval Price";
            this.evalPriceButton.UseVisualStyleBackColor = true;
            this.evalPriceButton.Click += new System.EventHandler(this.evalPriceButton_Click);
            // 
            // AddTradeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1416, 1322);
            this.Controls.Add(this.evalPriceButton);
            this.Controls.Add(this.stockInfoBox);
            this.Controls.Add(this.optionInfoBox);
            this.Controls.Add(this.saveToDbButton);
            this.Controls.Add(this.buyRadioButton);
            this.Controls.Add(this.barrierGroupBox);
            this.Controls.Add(this.sellRadioButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.quantityBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.instrumentComboBox);
            this.Controls.Add(this.tradedPriceBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.effectiveDatePicker);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tradeIdBox);
            this.Controls.Add(this.label1);
            this.Name = "AddTradeForm";
            this.Text = "Add New Trade";
            this.barrierGroupBox.ResumeLayout(false);
            this.barrierGroupBox.PerformLayout();
            this.digitalGroupBox.ResumeLayout(false);
            this.digitalGroupBox.PerformLayout();
            this.optionInfoBox.ResumeLayout(false);
            this.optionInfoBox.PerformLayout();
            this.stockInfoBox.ResumeLayout(false);
            this.stockInfoBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tradedPriceBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker effectiveDatePicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tradeIdBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox instrumentComboBox;
        private System.Windows.Forms.TextBox quantityBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton sellRadioButton;
        private System.Windows.Forms.RadioButton buyRadioButton;
        private System.Windows.Forms.Button saveToDbButton;
        private System.Windows.Forms.GroupBox barrierGroupBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton barrierDownOut;
        private System.Windows.Forms.RadioButton barrierDownIn;
        private System.Windows.Forms.RadioButton barrierUpOut;
        private System.Windows.Forms.RadioButton barrierUpIn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox barrierValueBox;
        private System.Windows.Forms.Label barrierLabel;
        private System.Windows.Forms.GroupBox digitalGroupBox;
        private System.Windows.Forms.Label digitalOptionLabel;
        private System.Windows.Forms.TextBox digitalRebateBox;
        private System.Windows.Forms.Label digitalLabel;
        private System.Windows.Forms.TextBox optionExpirationBox;
        private System.Windows.Forms.TextBox optionStrikeBox;
        private System.Windows.Forms.RadioButton optionIsCall;
        private System.Windows.Forms.RadioButton optionIsPut;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox optionInfoBox;
        private System.Windows.Forms.TextBox optionIdBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox optionTypeBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox stockInfoBox;
        private System.Windows.Forms.TextBox stockId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox stockBBG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox stockCompany;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button evalPriceButton;
    }
}