﻿using System;
using System.Collections.Generic;
using System.Linq;
using Database;
using FinanceLib.DataManager;
using FinanceLib.FinancialCalculators;
using FinanceLib.Instruments;
using FinanceLib.TradeManager;

namespace DRTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //FM5022_HW2();
            FM5022_HW3();

            Console.WriteLine("done.");
            Console.Read();
        }

        private static void FM5022_HW3()
        {
            var spot = 10;
            var k = 12;
            var t = 0.5;
            var r = 0.02;
            var div = 0.0;
            var vol = 0.18;
            var isCall = true;
            var numSteps = 24;

            var americanOption = new EuropeanOption(spot, k, t, r, div, vol, isCall);
            Console.WriteLine("Price: " + TrinomialTree.CalcPrice(americanOption, numSteps));
            Console.WriteLine("Delta: " + TrinomialTree.CalcDelta(spot, k, t, r, div, vol, isCall, false, numSteps));
            Console.WriteLine("Gamma: " + TrinomialTree.CalcGamma(spot, k, t, r, div, vol, isCall, false, numSteps));
            Console.WriteLine("Theta: " + TrinomialTree.CalcTheta(spot, k, t, r, div, vol, isCall, false, numSteps));
        }

        private static void FM5022_HW2()
        {
            var spot = 30;
            var k = 34;
            var t = 0.5;
            var r = 0.05;
            var div = 0.05;
            var vol = 0.25;
            var isCall = true;
            var numSteps = 1;

            var euroOption = new EuropeanOption(spot, k, t, r, div, vol, isCall);
            Console.WriteLine("Problem 4");
            Console.WriteLine("Black Scholes Price");
            Console.WriteLine(BlackScholes.CalcPrice(euroOption) + "\n\n");

            var senarioList = new List<int> { 1000, 5000, 10000 };


            foreach (var numScenarios in senarioList)
            {
                var antithetic = false;
                var scenarios = MonteCarloCalculator.GenerateScenarioSet(euroOption, numSteps, false, numScenarios, out var rndNums);
                var res = MonteCarloCalculator.CalcPriceList(euroOption, scenarios, numSteps);
                DisplayRun(numScenarios, antithetic, res.Average(), FinanceCalculators.StndErr(res));

                antithetic = true;
                scenarios = MonteCarloCalculator.GenerateScenarioSet(euroOption, numSteps, antithetic, rndNums);
                res = MonteCarloCalculator.CalcPriceList(euroOption, scenarios, numSteps);
                DisplayRun(numScenarios, antithetic, res.Average(), FinanceCalculators.StndErrAntithetic(res));
            }


            spot = 20;
            t = 0.5;
            r = 0.04;
            div = 0.0;
            var vol1 = 0.25;
            var vol2 = 0.25;
            var correlation = 0.5;
            var numDigiScenarios = 1000000;
            var digiList = new List<CorrelatedDigiOption>();
            for (var i = 0; i < numDigiScenarios; i++)
                digiList.Add(new CorrelatedDigiOption(t, r, div, vol1, vol2, correlation, numSteps));

            var res4 = new List<double>();
            foreach (var digi in digiList)
                res4.Add(digi.CalcDigiPrice(spot, 22.5, 17.50));
            Console.WriteLine("Problem 5");
            Console.WriteLine("Digital Option on two stocks");
            DisplayRun(numDigiScenarios, false, res4.Average(), FinanceCalculators.StndErr(res4));
        }

        private static void DisplayRun(double numScenarios, bool useAnti, double price, double variance)
        {
            Console.WriteLine("NumScenarios = " + numScenarios + "\tUsing antithetic reduction: " + useAnti);
            Console.WriteLine("Option Price = " + price);
            Console.WriteLine("Variance = " + variance);
            Console.WriteLine("\n");
        }

        private static void PopulateTestDB()
        {
            if (true)
                ClearAllData();

            PopulateRateData();
            PopulateSpotData();
            CreateStocksAndOptions();
            PopulateTradeData();

            var testList = new List<Instrument>();
            var trade = TradeManager.LoadAllTrades().Single(r => r.TradeId == 4);
            testList.Add(InstrumentUtils.LoadInstrument(3));
            testList.Add(InstrumentUtils.LoadInstrument(8));
            testList.Add(trade.Instrument);

            foreach (var t in testList)
                Console.WriteLine(t.GetDisplayName());
        }

        private static void PopulateSpotData()
        {
            var uploadDict = new Dictionary<DateTime, Dictionary<string, double>>();

            var data = new Dictionary<string, double>() {
                {"SPX", 2669.91},
                {"GOOGL", 1031.45},
                {"NQ1", 6669.75},
            };
            var date = DateTime.Today;
            uploadDict.Add(date, data);

            data = new Dictionary<string, double>() {
                {"SPX", 2600.12},
                {"GOOGL", 1053.11},
                {"NQ1", 6520.32},
            };
            date = date.AddDays(-1);
            uploadDict.Add(date, data);


            data = new Dictionary<string, double>() {
                {"SPX", 2650.32},
                {"GOOGL", 1000.1},
                {"NQ1", 6801.4},
            };
            date = date.AddDays(-1);
            uploadDict.Add(date, data);

            data = new Dictionary<string, double>() {
                {"SPX", 2539.3},
                {"GOOGL", 1100.2},
                {"NQ1", 6737.98},
            };
            date = date.AddDays(-1);
            uploadDict.Add(date, data);

            MarketDataManager.AddSpots(DateTime.Now, uploadDict);
        }

        private static void PopulateRateData()
        {
            var data = new Dictionary<int, double>() {
                {1, 0.05},
                {2, 0.045},
                {5, 0.03},
                {10, 0.029},
                {15, 0.025},
            };
            var date = DateTime.Today;
            MarketDataManager.AddRateCurveData(date, data);

            data = new Dictionary<int, double>() {
                {1, 0.07},
                {2, 0.05},
                {5, 0.041},
                {10, 0.03},
                {15, 0.025},
            };
            date = date.AddDays(-2);
            MarketDataManager.AddRateCurveData(date, data);
        }

        private static void PopulateTradeData()
        {
            var testList = new List<Trade>();
            testList.Add(new Trade(1, DateTime.Today, 205, 100, true));
            testList.Add(new Trade(5, DateTime.Today, 1505, 100, false));
            testList.Add(new Trade(8, DateTime.Today, 5, 50, false));
            testList.Add(new Trade(7, DateTime.Today, 6671, 50, true));

            foreach (var t in testList)
                t.SaveToDb();
        }

        private static void CreateStocksAndOptions()
        {
            var testList = new List<Instrument>();

            var euroOption = new EuropeanOption(1050, 1050, 1, 0.05, 0, 0.5, true, "GOOGL", DateTime.Today.AddYears(1));
            var americanOption1 = new AmericanOption(2660, 2660, 1, 0.05, 0, 0.5, true, "SPX", DateTime.Today.AddYears(3));
            var americanOption2 = new AmericanOption(1000, 1000, 1, 0.05, 0, 0.5, true, "GOOGL", DateTime.Today.AddYears(6));
            var barrierOption = new BarrierOption(6700, 6700, 1, 0.05, 0, 0.5, true, 55, true, true, "NQ1", DateTime.Today.AddYears(1));
            var digitalOption = new DigitalOption(2500, 2500, 1, 0.05, 0, 0.5, true, 5, "SPX", DateTime.Today.AddYears(1));

            testList.Add(euroOption);
            testList.Add(americanOption1);
            testList.Add(new Stock("GOOGL", "Google Inc", "GOOGL", "Very big tech"));
            testList.Add(americanOption2);
            testList.Add(barrierOption);
            testList.Add(new Stock("SPX", "SnP 500 Index", "SPX", "Index o' big companies"));
            testList.Add(new Stock("NQ1", "NASDAQ futures", "NQ1", "Futures on very big companies"));
            testList.Add(digitalOption);

            foreach (var t in testList)
                t.SaveToDb();
        }

        private static void ClearAllData()
        {
            using (var db = new InstrumentDB())
            {
                var clearData1 = db.Db_Instrument;
                if (clearData1 != null)
                    db.Db_Instrument.RemoveRange(clearData1);

                var clearData2 = db.Inst_Option;
                if (clearData2 != null)
                    db.Inst_Option.RemoveRange(clearData2);

                var clearData3 = db.Inst_Option_Digital;
                if (clearData3 != null)
                    db.Inst_Option_Digital.RemoveRange(clearData3);

                var clearData4 = db.Inst_Option_Barrier;
                if (clearData4 != null)
                    db.Inst_Option_Barrier.RemoveRange(clearData4);

                var clearData5 = db.Inst_Option_Lookback;
                if (clearData5 != null)
                    db.Inst_Option_Lookback.RemoveRange(clearData5);

                var clearData6 = db.Inst_Stock;
                if (clearData6 != null)
                    db.Inst_Stock.RemoveRange(clearData6);

                db.SaveChanges();
            }

            using (var mdDB = new MarketDB())
            {
                var clearData1 = mdDB.MD_Underlying;
                if (clearData1 != null)
                    mdDB.MD_Underlying.RemoveRange(clearData1);

                var clearData2 = mdDB.MD_SnapShot.ToList();
                if (clearData2 != null)
                    mdDB.MD_SnapShot.RemoveRange(clearData2);

                var clearData3 = mdDB.MD_InterestRate;
                if (clearData3 != null)
                    mdDB.MD_InterestRate.RemoveRange(clearData3);

                mdDB.SaveChanges();
            }

            using (var db = new PortfolioDB())
            {
                var clearData1 = db.Portfolio_Trades;
                if (clearData1 != null)
                    db.Portfolio_Trades.RemoveRange(clearData1);

                db.SaveChanges();
            }
        }
    }
}

