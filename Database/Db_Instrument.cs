//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Db_Instrument
    {
        public int InstrumentId { get; set; }
        public string DisplayType { get; set; }
        public string InstrumentType { get; set; }
        public string UnderlyingId { get; set; }
    }
}
